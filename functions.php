<?php
/**
 * PSR-2
 * functions.php
 *
 * This file is used to store nearly all functions used by Federama.
 *
 * since Federama version 0.1
 *
 */

include "conn.php";


// put this here for various functions to use
$dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
mysqli_set_charset($dbconn, "utf8");

$metadescription = _("<i>Federama</i> is an open-source PHP framework and blogging software for the Fediverse - a decentralized social network of thousands of different communities and millions of users.");

$mysiteq = "SELECT * FROM ".TBLPREFIX."configuration";
$mysitequery = mysqli_query($dbconn,$mysiteq);
while ($mysiteopt = mysqli_fetch_assoc($mysitequery)) {
    $website_url                    = $mysiteopt['website_url'];
    $website_name                   = $mysiteopt['website_name'];
    $website_description            = $mysiteopt['website_description'];
    $default_locale                 = $mysiteopt['default_locale'];
    $open_registration              = $mysiteopt['open_registrations'];
    $admin_account                  = $mysiteopt['admin_account'];
    $admin_email                    = $mysiteopt['admin_email'];
    $installed_themes               = $mysiteopt['installed_themes'];
    $active_theme                   = $mysiteopt['active_theme'];
    $blocked_instances              = $mysiteopt['blocked_instances'];
    $list_with_the_federation_info  = $mysiteopt['list_with_the_federation_info'];
    $list_with_fediverse_network    = $mysiteopt['list_with_fediverse_network'];
    $list_with_federama_social      = $mysiteopt['list_with_federama_social'];
}

/**
 *
 * TIME AND DATE FUNCTIONS
 */




// creates a 20 character ID
function makeid($newid) {
    // the characters we will use
    $chars	= "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    // splits $chars into an array of individual characters
    $tmp = preg_split("//u", $chars, -1, PREG_SPLIT_NO_EMPTY);

    // shuffles/randomizes the $tmp array
    shuffle($tmp);

    // turns the randomized array into a string
    $tmp2 = join("", $tmp);

    // returns the first 10 characters of the randomized string
    return mb_substr($tmp2,0,20,"UTF-8");
}




// sanitizes text inputs from forms
function nicetext($text) {
    // get rid of whitespace characters at start or end of text
    $text = trim($text);

    // removes \ backslash escape characters
    #$text = stripslashes($text);

    // converts special characters (i.e. < > &, etc) into their html entities
    $text = htmlspecialchars($text,ENT_QUOTES,'UTF-8',true);

   
    return $text;
}


// a quicker way of decoding html entities
function retext($text) {
    $text = html_entity_decode($text,ENT_QUOTES,'UTF-8');

    return $text;
}

// makes a url-friendly slug
function makeslug($text) {
    /**
     * taken from https://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes
     */

    // make all letters lowercase
    $text = strtolower($text);


    // clean up multiple hyphens and whitespaces
    //$text = preg_replace("/[\s-]+/", " ", $text);

    // convert whitespaces and underscores to hyphens
    $text = preg_replace("/[\s_]/", "-", $text);

    // make it alphanumeric and turn other characters into hyphens
    $text = preg_replace("/[^a-z0-9_\s-]/", "-", $text);

    return $text;
}



// turns a fileneame into something url-friendly, but keeps the . for the extension.
function makenicefile($text) {
    /**
     * taken from https://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes
     */

    // make all letters lowercase
    $text = strtolower($text);

    // make it alphanumeric and turn other characters into hyphens
    $text = preg_replace("/[^a-z0-9\.]/", "-", $text);

    // clean up multiple hyphens and whitespaces
    //$text = preg_replace("/[\s-]+/", " ", $text);

    // convert whitespaces and underscores to hyphens
    $text = preg_replace("/[\s_]/", "-", $text);

    return $text;
}


// cleans up strings of tags, categories, collections, etc
function makecoll($text) {

    // convert colons to commas
    $text = preg_replace('/:/i', ',', $text);

    // convert semicolons to commas
    $text = preg_replace('/;/i', ',', $text);

    // remove spaces directly after commas
    $text = preg_replace('/,( +)/i', ',', $text);

    // converts special characters (i.e. < > &, etc) into their html entities
    $text = htmlspecialchars($text,ENT_QUOTES,'UTF-8',true);

    return $text;
}



// finds @usernames and turns them into links
function profileparser($text) {

    // link will be something like <a href='http://example.tld/users/username'>@username</a>
    $newtext = preg_replace('/(@([a-zA-Z0-9]+))/i', '<a href=\''.$website_url.'users/\2\'>\1</a>',$text);
    return $newtext;

}

// displays a warning notice on a page
function warning_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class warning notice is for warnings -->\n";
    $ntc .= "\t<div class=\"warning-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays an error notice on a page
function error_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class error notice is for errors -->\n";
    $ntc .= "\t<div class=\"error-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a important notice on a page
function important_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class important notice is for general messages & warnings -->\n";
    $ntc .= "\t<div class=\"important-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a banned account notice on a page
function banned_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class banned notice is for banned users -->\n";
    $ntc .= "\t<div class=\"banned-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a suspended account notice on a page
function suspended_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class suspended notice is for suspended accounts -->\n";
    $ntc .= "\t<div class=\"suspended-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a generic notice on a page
function notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class notice is for general notices & warnings -->\n";
    $ntc .= "\t<div class=\"notice\">".$notice."</div>\n";

    return $ntc;
}

// redirects to another page
function redirect($location) {
    return header("Location: $location");
}

// get the date of birth, return the age
function user_age($userage) {
    return floor((time() - strtotime($userage))/31556926);
}

// strip the protocol from a url
function short_url($url) {
    return preg_replace('/(https:\/\/)|(http:\/\/)/i', '', $url);
}

// make time differences nice looking
# I could never get the code working correctly. It will have to wait until a future release.

 // get the number of users
 function user_quantity($users) {
     $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
     $userqq = "SELECT * FROM ".TBLPREFIX."users";
     $userqquery = mysqli_query($dbconn,$userqq);
     $userqty = mysqli_num_rows($userqquery);

     return $userqty;
 }

 // get the number of active users over the past six months
function users_half_year($sometimes_users) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

    $usershalfyear = 0;

    $usershalfyearq = "SELECT * FROM ".TBLPREFIX."users";
    $usershalfyearquery = mysqli_query($dbconn,$usershalfyearq);
    while ($usershalfyearopt = mysqli_fetch_assoc($usershalfyearquery)) {
        $lastlogin  = strtotime($usershalfyearopt['user_last_login']);
        $now        = strtotime('now');
        if (($now - $lastlogin) < 15778800) { // 15778800 is six months in seconds
            $usershalfyear++;
        }
    }

    return $usershalfyear;
}

 // get the number of active users over the past month
 function users_past_month($active_users) {
     $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

     $usersmonthqty = 0;

     $usersmonthq = "SELECT * FROM ".TBLPREFIX."users";
     $usersmonthquery = mysqli_query($dbconn,$usersmonthq);
     while ($usersmonthopt = mysqli_fetch_assoc($usersmonthquery)) {
         $lastlogin = strtotime($usersmonthopt['user_last_login']);
         $now       = strtotime('now');
         if (($now - $lastlogin) < 2629800) { // 2629800 is one month in seconds
             $usersmonthqty++;
         }
     }

     return $usersmonthqty;
 }

 // get the number of posts
 function post_quantity($posts) {
     $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
     $postqq = "SELECT * FROM ".TBLPREFIX."posts WHERE post_type='POST'";
     $postqquery = mysqli_query($dbconn,$postqq);
     $postqty = mysqli_num_rows($postqquery);

     return $postqty;
 }

 // get the number of pages
 function page_quantity($pages) {
     $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
     $pageqq = "SELECT * FROM ".TBLPREFIX."posts WHERE post_type='PAGE'";
     $pageqquery = mysqli_query($dbconn,$pageqq);
     $pageqty = mysqli_num_rows($pageqquery);

     return $pageqty;
 }


// get the number of messages for the Dashboard
function message_quantity($messages) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $messqq = "SELECT * FROM ".TBLPREFIX."messages";
    $messqquery = mysqli_query($dbconn,$messqq);
    $messqty = mysqli_num_rows($messqquery);

    return $messqty;
}


// get the number of images for the Dashboard
function image_quantity($images) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $imageqq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE'";
    $imageqquery = mysqli_query($dbconn,$imageqq);
    $imageqty = mysqli_num_rows($imageqquery);

    return $imageqty;
}


// get the number of audio files for the Dashboard
function audio_quantity($audios) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $audioqq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='AUDIO'";
    $audioqquery = mysqli_query($dbconn,$audioqq);
    $audioqty = mysqli_num_rows($audioqquery);

    return $audioqty;
}


// get the number of video files for the Dashboard
function video_quantity($videos) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $videoqq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='VIDEO'";
    $videoqquery = mysqli_query($dbconn,$videoqq);
    $videoqty = mysqli_num_rows($videoqquery);

    return $videoqty;
}


// get the number of documents for the Dashboard
function document_quantity($documents) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $documentqq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='DOCUMENT'";
    $documentqquery = mysqli_query($dbconn,$documentqq);
    $documentqty = mysqli_num_rows($documentqquery);

    return $documentqty;
}


// get the number of package files for the Dashboard
function package_quantity($packages) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $packageqq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='PACKAGE'";
    $packageqquery = mysqli_query($dbconn,$packageqq);
    $packageqty = mysqli_num_rows($packageqquery);

    return $packageqty;
}

// get the number of posts, pages, and messages for Nodeinfo
function nodeinfo_posts($nodeposts) {
    /**
     * For Nodeinfo, the number of "posts" will equal the number of posts, pages,
     * comments, messages, and replies.
     */


}

// Get the number of a user's posts from user_outbox in users table
function user_post_quantity($userid) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $postsbyq = "SELECT * FROM ".TBLPREFIX."posts WHERE user_id='".$userid."'";
    $postsbyquery = mysqli_query($dbconn,$postsbyq);
    $postsbyqty = mysqli_num_rows($postsbyquery);

    return $postsbyqty;
}

// get the number of the user's followers

// get the number of accounts the user is following

// get the date of the latest post for the Atom feed
function atom_updated($time) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    $postqq = "SELECT * FROM ".TBLPREFIX."posts WHERE post_status=\"6ьötХ5áзÚZ\" ORDER BY post_date DESC LIMIT 1";
    $postqquery = mysqli_query($dbconn,$postqq);
    while ($postopt = mysqli_fetch_assoc($postqquery)) {
        $updated = $postopt['post_date'];
        return date("c", strtotime($updated));
    }
}
