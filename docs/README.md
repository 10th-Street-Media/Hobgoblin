# **Hobgoblin** Documentation


Since the software is still being developed, the documentation process will be haphazard and inconsistent, at least until version 1.0.

### Notes
These notes are mostly for myself to keep track of what needs to be done in the short term.

data-fill:
    could not add data to hob_id3_genres

dashboard
    Remove Recent Posts
    Add Recent Media
    Quick Stats
        Number of artists
        Number of companies
        Number of collections
        Number of categories
        Number of tags

the-media
    test out video files
    test out audio files
