# Hobgoblin

**Hobgoblin** is a software platform for managing media collections and playing the content in them. It's inspired by software such as _Kodi_, _Songbird_, _Pix_, and _Calibre_. All of these programs are pretty good at what they do, but they're focused too narrowly, and lack the ability to multiple media types, even if they're related.

As an example, let's say I'm a big fan of William Shakespeare, and suppose I'm reading an e-book of _Henry V_. After I finish the book, it would be nice if I could easily watch Kenneth Branagh's film. There should be an easier way to do it than opening yet another program or starting yet another device.

Not convinced? Suppose you are a fan of the **MASSIVE MEDIA FRANCHISE**. You know, the one with the books, the movies, the award-winning soundtrack. Don't forget the comic books (_ahem... graphic novels_). There's even talk about a TV series. How do you think they'll work that into the canon? Wouldn't it be great if you could keep all the media in one spot? Maybe they can license their IP to a PC maker so you can own the _official_ computer of the **MASSIVE MEDIA FRANCHISE**.

With Hobgoblin, you can note the actors and directors, and keep track of their filmographies. You can sort all your media into the same genres. I've already got my growing collection of _Scandanavian Noir_.

Hobgoblin is built around _collections_. Tags and categories are examples of collections, but it also includes artists, companies, and even a generic catchall called _collections_. These various types of collections make it easy to cross-reference stories, characters, artists, etc., across a variety of media.
