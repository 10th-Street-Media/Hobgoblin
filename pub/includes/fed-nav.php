<?php
/* PSR-2
 * pub/includes/fed-nav.php
 *
 * This is a navigation menu for some public facing webpages in Federama.
 *
 * since Hobgoblin version 0.1
 *
 */

 // get number of posts
# this function below isn't working at the moment. Try the variables below that.
# $postsbyqty = user_post_quantity($userid);
$postsbyq = "SELECT * FROM ".TBLPREFIX."posts WHERE user_id='".$userid."'";
$postsbyquery = mysqli_query($dbconn,$postsbyq);
$postsbyqty = mysqli_num_rows($postsbyquery);

// get number of followers and number of accounts being followed
$followersq = "SELECT * FROM ".TBLPREFIX."users where user_id='".$userid."'";
$followersquery = mysqli_query($dbconn,$followersq);
while ($followersopt = mysqli_fetch_assoc($followersquery)) {
    $followersqty = count(explode(",",$followersopt['user_followers']));
    $followingqty = count(explode(",",$followersopt['user_follows']));
}

?>
    <!-- THE CONTAINER for the main content -->
    <main class="w3-container" style="margin-top:40px;">

        <!-- THE GRID -->
        <div class="w3-row">

            <nav class="w3-col w3-panel w3-cell m2">
                <div class="w3-card-2 w3-theme-d5 w3-padding">
                    <button onclick="menuFunc('imagesSingle')" class="w3-button w3-block w3-left-align">Images</button>
                    <div id="imagesSingle" class="w3-container w3-hide">
                        <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=titles">Titles</a>
                        <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=artists">Artists</a>
                        <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=companies">Companies</a>
                        <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=tags">Tags</a>
                        <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=categories">Categories</a>
                        <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=ratings">Ratings</a>
                        <!-- <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=decades">Decades</a> -->
                        <!-- <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=sizes">Sizes</a>  -->
                        <!-- <a class="w3-button w3-block w3-left-align" href="<?php echo $website_url; ?>images.php?sort=layouts">Layouts</a>  -->
                    </div>

                    <button onclick="menuFunc('imagesGroup')" class="w3-button w3-block w3-left-align">Image Galleries</button>
                    <div id="imagesGroup" class="w3-container w3-hide">
                        <a class="w3-button w3-block w3-left-align" href="#">Titles</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Categories</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Artists</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Years</a>
                    </div>

                    <button onclick="menuFunc('audioSingle')" class="w3-button w3-block w3-left-align">Songs</button>
                    <div id="audioSingle" class="w3-container w3-hide">
                        <a class="w3-button w3-block w3-left-align" href="#">Titles</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Artists</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Genres</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Lengths</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Decades</a>
                    </div>

                    <button onclick="menuFunc('audioGroup')" class="w3-button w3-block w3-left-align">Albums</button>
                    <div id="audioGroup" class="w3-container w3-hide">
                        <a class="w3-button w3-block w3-left-align" href="#">Titles</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Artists</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Genres</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Years</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Recent</a>
                    </div>

                    <button onclick="menuFunc('videoSingle')" class="w3-button w3-block w3-left-align">Videos</button>
                    <div id="videoSingle" class="w3-container w3-hide">
                        <a class="w3-button w3-block w3-left-align" href="#">Titles</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Artists</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Tags</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Lengths</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Decades</a>
                    </div>

                    <button onclick="menuFunc('videoGroup')" class="w3-button w3-block w3-left-align">Video Series</button>
                    <div id="videoGroup" class="w3-container w3-hide">
                        <a class="w3-button w3-block w3-left-align" href="#">Titles</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Artists</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Genres</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Years</a>
                        <a class="w3-button w3-block w3-left-align" href="#">Recent</a>
                    </div>
                    <button class="w3-button w3-block w3-left-align"><a href="#">Artists</a></button>
                    <button class="w3-button w3-block w3-left-align"><a href="#">Companies</a></button>
                </div>
            </nav>

<script>
function menuFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else {
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>
<?php

// messages should appear in <main> only, not in <nav>
if ($message != '' || NULL) {
    echo header_message($message);
}
?>
