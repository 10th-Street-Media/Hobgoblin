<?php
/**
 * pub/images.php
 *
 * Displays images, sorted in various ways
 *
 * since Hobgoblin version 0.1
 */

include_once    "../conn.php";
include         "../functions.php";
require         "includes/database-connect.php";
require_once    "includes/configuration-data.php";
include_once    "nodeinfo/version.php";

/**
 * Number of items per page
 */
 $perpg = $frontend_index_items;

if(isset($_GET["pg"])){
    $pg = intval($_GET["pg"]);
} else {
    $pg = 1;
}

$calc = $perpg * $pg;
$start = $calc - $perpg;

// get the sort order, which will determine what is displayed and how
if (isset($_GET["sort"])) {
    if ($_GET["sort"] == "titles") {
        $sort = "titles";
        $pagetitle = _("Images sorted by title « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' ORDER BY media_title_sort_order ASC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "artists") {
        $sort = "artists";
        $pagetitle = _("Images sorted by artist « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_artist!='' ORDER BY media_artist ASC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "companies") {
        $sort = "companies";
        $pagetitle = _("Images sorted by company « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_company!='' ORDER BY media_company ASC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "tags") {
        $sort = "tags";
        $pagetitle = _("Images sorted by tag « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_tags!='' ORDER BY media_tags ASC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "categories") {
        $sort = "categories";
        $pagetitle = _("Images sorted by category « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_categories!='' ORDER BY media_categories ASC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "ratings") {
        $sort = "ratings";
        $pagetitle = _("Images sorted by rating « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_rating!='0000' ORDER BY media_rating DESC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "decades") {
        $sort = "decades";
        $pagetitle = _("Images sorted by decade « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_year!='' ORDER BY media_year DESC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "sizes") {
        $sort = "sizes";
        $pagetitle = _("Images sorted by size « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' ORDER BY media_date DESC LIMIT $start, $perpg";
    } else if ($_GET["sort"] == "layouts") {
        $sort = "layouts";
        $pagetitle = _("Images sorted by layout « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' AND media_layout!='' ORDER BY media_layout ASC LIMIT $start, $perpg";
    } else {
        $pagetitle = _("Latest images « $website_name « ɧobgoblin");
        $imageq = "SELECT * FROM ".TBLPREFIX."media WHERE media_type='IMAGE' AND media_status='PUBLIC' ORDER BY media_date DESC LIMIT $start, $perpg";
    }
}


include_once "includes/fed-header.php";
include_once "includes/fed-nav.php";
?>


            <article class="w3-padding w3-col s12 m8 l10">
            <h2 class="w3-padding"><?php echo _("Media library"); ?></h2>
<?php

if ($sort === "titles" || $sort === "artists" || $sort === "companies") {


    echo "\t\t\t\t<table class=\"w3-table-all w3-hoverable w3-margin-left\">\n";

    echo "\t\t\t\t\t<tr class=\"w3-theme-dark\">\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Title')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Year')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Artist')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Company')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Gallery')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Rating')."</th>\n";
    echo "\t\t\t\t\t</tr>\n";

    // get the images information from the database, based on the query selected above
    $imagequery = mysqli_query($dbconn,$imageq);
    $imagequeryrows = mysqli_num_rows($imagequery);
    if ($imagequeryrows) {

        $i = 0;

        while($imageopt = mysqli_fetch_assoc($imagequery)) {
            $mediaid        = $imageopt['media_id'];
            $mediauser      = $imageopt['user_name'];
            $mediadate      = $imageopt['media_date'];
            $mediaurl       = $imageopt['media_url'];
            $mediatitle     = $imageopt['media_title'];
            $mediaslug      = $imageopt['media_slug'];
            $mediaalt       = $imageopt['media_alt'];
            $mediaarti      = preg_split('/,/i', $imageopt['media_artist']);
            $mediacomp      = preg_split('/,/i', $imageopt['media_company']);
            $mediayear      = $imageopt['media_year'];
            $mediatags      = preg_split('/,/i', $imageopt['media_tags']);
            $mediacats      = preg_split('/,/i', $imageopt['media_categories']);
            $mediarate      = $imageopt['media_rating'];
            $mediawide      = $imageopt['media_image_width'];
            $mediatall      = $imageopt['media_image_height'];
            $medialayo      = $imageopt['media_layout'];
            $mediacoll      = preg_split('/,/i', $imageopt['media_collection_id']);
            $mediatitlesort = $imageopt['media_title_sort_order'];
            $mediawarn     = $imageopt['media_content_warning'];

            switch ($mediarate) {
                case "0000":
                    $rating = "☆☆☆☆☆☆☆☆☆☆";
                    break;

                case "0001":
                    $rating = "★☆☆☆☆☆☆☆☆☆";
                    break;

                case "0002":
                    $rating = "★★☆☆☆☆☆☆☆☆";
                    break;

                case "0003":
                    $rating = "★★★☆☆☆☆☆☆☆";
                    break;

                case "0004":
                    $rating = "★★★★☆☆☆☆☆☆";
                    break;

                case "0005":
                    $rating = "★★★★★☆☆☆☆☆";
                    break;

                case "0006":
                    $rating = "★★★★★★☆☆☆☆";
                    break;

                case "0007":
                    $rating = "★★★★★★★☆☆☆";
                    break;

                case "0008":
                    $rating = "★★★★★★★★☆☆";
                    break;

                case "0009":
                    $rating = "★★★★★★★★★☆";
                    break;

                case "0010":
                    $rating = "★★★★★★★★★★";
                    break;
            }


            // display a row with the information
            echo "\t\t\t\t\t<tr class=\"w3-theme-dark\">\n";
            echo "\t\t\t\t\t\t<td>\n";
            echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$mediaurl."\" width=\"50px\" height=\"50px\"";
            if ($mediawarn == "1") {
                echo " style=\"filter:blur(3px)\"";
            }
            echo "></a>\n";
            echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\">".$mediatitle."</a>\n";
            echo "\t\t\t\t\t\t</td>\n";
            echo "\t\t\t\t\t\t<td>\n";
            echo "\t\t\t\t\t\t\t".$mediayear."\n";
            echo "\t\t\t\t\t\t</td>\n";
            echo "\t\t\t\t\t\t<td>\n";
            echo "\t\t\t\t\t\t\t";
            foreach ($mediaarti as $artist) {
                $artslug = makeslug($artist);
                echo "<a href=\"".$website_url."artist/".$artslug."\">".$artist."</a> ";
            }
            echo "\n";
            echo "\t\t\t\t\t\t</td>\n";
            echo "\t\t\t\t\t\t<td>\n";
            echo "\t\t\t\t\t\t\t";
            foreach ($mediacomp as $company) {
                $compslug = makeslug($company);
                echo "<a href=\"".$website_url."company/".$compslug."\">".$company."</a> ";
            }
            echo "\n";
            echo "\t\t\t\t\t\t</td>\n";
            echo "\t\t\t\t\t\t<td>\n";
            echo "\t\t\t\t\t\t\t";
            foreach ($mediacoll as $gallery) {
                $gallslug = makeslug($gallery);
                echo "<a href=\"".$website_url."collection/".$gallslug."\">".$gallery."</a> ";
            }
            echo "\n";
            echo "\t\t\t\t\t\t</td>\n";
            echo "\t\t\t\t\t\t<td>\n";
            echo "\t\t\t\t\t\t\t".$rating."\n";
            echo "\t\t\t\t\t\t</td>\n";
            echo "\t\t\t\t\t</tr>\n";
        }
    }
    echo "\t\t\t\t</table>\n";

} else if ($sort == "tags") {

    $tagsq = "SELECT * FROM ".TBLPREFIX."tags ORDER BY tag_sort_name ASC";
    $tagsquery = mysqli_query($dbconn,$tagsq);
    while ($tagsopt = mysqli_fetch_assoc($tagsquery)) {
        $tgname     = $tagsopt['tag_name'];
        $tgslug     = $tagsopt['tag_slug'];
        $tgsort     = $tagsopt['tag_sort_name'];
        $tgdesc     = $tagsopt['tag_description'];

        echo "\t\t\t\t\t<h4 class=\"w3-padding\">".$tgname."</h4>\n";

        echo "\t\t\t\t<table class=\"w3-table-all w3-hoverable w3-margin-left\">\n";
        echo "\t\t\t\t\t<tr class=\"w3-theme-dark\">\n";
        echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Title')."</th>\n";
        echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Year')."</th>\n";
        echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Artist')."</th>\n";
        echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Company')."</th>\n";
        echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Gallery')."</th>\n";
        echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Rating')."</th>\n";
        echo "\t\t\t\t\t</tr>\n";

        $imagequery = mysqli_query($dbconn,$imageq);
        $imagequeryrows = mysqli_num_rows($imagequery);
        if ($imagequeryrows) {

            while($imageopt = mysqli_fetch_assoc($imagequery)) {
                $mediaid        = $imageopt['media_id'];
                $mediauser      = $imageopt['user_name'];
                $mediadate      = $imageopt['media_date'];
                $mediaurl       = $imageopt['media_url'];
                $mediatitle     = $imageopt['media_title'];
                $mediaslug      = $imageopt['media_slug'];
                $mediaalt       = $imageopt['media_alt'];
                $mediaarti      = preg_split('/,/i', $imageopt['media_artist']);
                $mediacomp      = preg_split('/,/i', $imageopt['media_company']);
                $mediayear      = $imageopt['media_year'];
                $mediatags      = preg_split('/,/i', $imageopt['media_tags']);
                $mediacats      = preg_split('/,/i', $imageopt['media_categories']);
                $mediarate      = $imageopt['media_rating'];
                $mediawide      = $imageopt['media_image_width'];
                $mediatall      = $imageopt['media_image_height'];
                $medialayo      = $imageopt['media_layout'];
                $mediacoll      = preg_split('/,/i', $imageopt['media_collection_id']);
                $mediatitlesort = $imageopt['media_title_sort_order'];
                $mediawarn     = $imageopt['media_content_warning'];

                switch ($mediarate) {
                    case "0000":
                        $rating = "☆☆☆☆☆☆☆☆☆☆";
                        break;

                    case "0001":
                        $rating = "★☆☆☆☆☆☆☆☆☆";
                        break;

                    case "0002":
                        $rating = "★★☆☆☆☆☆☆☆☆";
                        break;

                    case "0003":
                        $rating = "★★★☆☆☆☆☆☆☆";
                        break;

                    case "0004":
                        $rating = "★★★★☆☆☆☆☆☆";
                        break;

                    case "0005":
                        $rating = "★★★★★☆☆☆☆☆";
                        break;

                    case "0006":
                        $rating = "★★★★★★☆☆☆☆";
                        break;

                    case "0007":
                        $rating = "★★★★★★★☆☆☆";
                        break;

                    case "0008":
                        $rating = "★★★★★★★★☆☆";
                        break;

                    case "0009":
                        $rating = "★★★★★★★★★☆";
                        break;

                    case "0010":
                        $rating = "★★★★★★★★★★";
                        break;
                }

                foreach ($mediatags as $tag) {
                    if ($tag == $tgname) {
                        // display a row with the information
                        echo "\t\t\t\t\t<tr class=\"w3-theme-dark\">\n";
                        echo "\t\t\t\t\t\t<td>\n";
                        echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$mediaurl."\" width=\"50px\" height=\"50px\"";
                        if ($mediawarn == "1") {
                            echo " style=\"filter:blur(3px)\"";
                        }
                        echo "></a>\n";
                        echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\">".$mediatitle."</a>\n";
                        echo "\t\t\t\t\t\t</td>\n";
                        echo "\t\t\t\t\t\t<td>\n";
                        echo "\t\t\t\t\t\t\t".$mediayear."\n";
                        echo "\t\t\t\t\t\t</td>\n";
                        echo "\t\t\t\t\t\t<td>\n";
                        echo "\t\t\t\t\t\t\t";
                        foreach ($mediaarti as $artist) {
                            $artslug = makeslug($artist);
                            echo "<a href=\"".$website_url."artist/".$artslug."\">".$artist."</a> ";
                        }
                        echo "\n";
                        echo "\t\t\t\t\t\t</td>\n";
                        echo "\t\t\t\t\t\t<td>\n";
                        echo "\t\t\t\t\t\t\t";
                        foreach ($mediacomp as $company) {
                            $compslug = makeslug($company);
                            echo "<a href=\"".$website_url."company/".$compslug."\">".$company."</a> ";
                        }
                        echo "\n";
                        echo "\t\t\t\t\t\t</td>\n";
                        echo "\t\t\t\t\t\t<td>\n";
                        echo "\t\t\t\t\t\t\t";
                        foreach ($mediacoll as $gallery) {
                            $gallslug = makeslug($gallery);
                            echo "<a href=\"".$website_url."collection/".$gallslug."\">".$gallery."</a> ";
                        }
                        echo "\n";
                        echo "\t\t\t\t\t\t</td>\n";
                        echo "\t\t\t\t\t\t<td>\n";
                        echo "\t\t\t\t\t\t\t".$rating."\n";
                        echo "\t\t\t\t\t\t</td>\n";
                        echo "\t\t\t\t\t</tr>\n";
                    }
                }

            }
            echo "\t\t\t\t</table>\n";

        }

    }

} else if ($sort == "categories") {

} else if ($sort == "ratings") {

} else if ($sort == "decades") {

} else if ($sort == "sizes") {

} else if ($sort == "layouts") {

    echo "\t\t\t\t\t<tr class=\"w3-theme-dark\">\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Title')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Artist')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Layout')."</th>\n";
    echo "\t\t\t\t\t</tr>\n";

} else { // sort by most recently added

    echo "\t\t\t\t\t<tr class=\"w3-theme-dark\">\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Title')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Artist')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Company')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Added by')."</th>\n";
    echo "\t\t\t\t\t\t<th class=\"w3-center\">"._('Added on')."</th>\n";
    echo "\t\t\t\t\t</tr>\n";
}


?>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
    if ($sort === "titles" || $sort === "artists" || $sort === "companies") {
        echo "\t\t\t<table cellspacing=\"2\" cellpadding=\"2\" align=\"center\">\n";
        echo "\t\t\t\t<tbody>\n";
        echo "\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t<td align=\"center\">\n";

        if (isset($pg)) {
            $pagecountquery = mysqli_query($dbconn,"SELECT COUNT(*) AS Total FROM ".TBLPREFIX."media");
            $pagecount = mysqli_num_rows($pagecountquery);

            if ($pagecount) {
                $rs = mysqli_fetch_assoc($pagecountquery);
                $total = $rs["Total"];
            }

            $totalPages = ceil($total / $perpg);

            if($pg <=1 ){
                echo "\t\t\t\t\t\t\t<span class=\"w3-padding\" style='font-weight: bold;'>Prev</span>\n";
            } else {
                $j = $pg - 1;
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$j&sort=$sort'>Prev</a></span>\n";
            }

            for($i=1; $i <= $totalPages; $i++) {
                if($i<>$pg) {
                    echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$i&sort=$sort'>$i</a></span>\n";
                } else {
                    echo "\t\t\t\t\t\t\t<span class=\"w3-button w3-green\" style='font-weight: bold;'>$i</span>\n";
                }
            } // end for $i=1

            if($pg == $totalPages ) {
                echo "\t\t\t\t\t\t\t<span class=\"w3-padding\" style='font-weight: bold;'>Next</span>\n";
            } else {
                $j = $pg + 1;
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$j&sort=$sort'>Next</a></span>\n";
            }
        }

        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t</tr>\n";
        echo "\t\t\t\t</tbody>\n";
        echo "\t\t\t</table>\n";
    }
?>

<?php
include_once "includes/fed-footer.php";
?>
