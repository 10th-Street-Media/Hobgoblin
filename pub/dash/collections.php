<?php
/*
 * pub/dash/collections.php
 *
 * A page listing all collections
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

$pagetitle = _("Collections « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Collections"); ?></h2>

                <a href="add-collection.php" class="w3-button w3-button-hover w3-theme-d3 w3-margin-left"><?php echo _("Add a collection"); ?></a><br><br>

                <table class="w3-table-all w3-hoverable w3-margin-left">
                    <tr class="w3-theme-dark">
                        <th class="w3-center"><?php echo _('Collection'); ?></th>
                        <th class="w3-center"><?php echo _('Description'); ?></th>
                        <th class="w3-center"><?php echo _('Actions'); ?></th>
                    </tr>


<?php
/**
 * Check the database to see what collections we have
 */
$getcollectionlistq = "SELECT * FROM ".TBLPREFIX."collections WHERE collection_name > '' ORDER BY collection_sort_name ASC";
$getcollectionlistquery = mysqli_query($dbconn,$getcollectionlistq);
while ($getcollectionlistopt = mysqli_fetch_assoc($getcollectionlistquery)) {
    $collectionid       = $getcollectionlistopt['collection_id'];
    $collectionname     = $getcollectionlistopt['collection_name'];
    $collectionslug     = $getcollectionlistopt['collection_slug'];
    $collectionsort     = $getcollectionlistopt['collection_sort_name'];
    $collectiondesc     = $getcollectionlistopt['collection_description'];
    $collectionavtr     = urldecode($getcollectionlistopt['collection_avatar_url']);

    echo "\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."collection/".$collectionslug."\"><img src=\"".$website_url.$collectionavtr."\" class=\"dash-avatar\"></a>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."collection/".$collectionslug."\">".$collectionname."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t".$collectiondesc."\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/edit-collection.php?cid=".$collectionid."\">"._('Edit')."</a>\n";
    echo "\t\t\t\t\t\t\t&nbsp;|&nbsp;\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/delete-collection.php?cid=".$collectionid."\">"._('Delete')."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t</tr>\n";
}
?>
                </table>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
