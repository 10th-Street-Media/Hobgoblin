<?php
/*
 * pub/dash/tags.php
 *
 * A page listing all tags on this instance.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

$pagetitle = _("Tags « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Tags"); ?></h2>

                <a href="add-tag.php" class="w3-button w3-button-hover w3-theme-d3 w3-margin-left"><?php echo _("Add a tag"); ?></a><br><br>

                <table class="w3-table-all w3-hoverable w3-margin-left">
                    <tr class="w3-theme-dark">
                        <th class="w3-center"><?php echo _('Tag'); ?></th>
                        <th class="w3-center"><?php echo _('Description'); ?></th>
                        <th class="w3-center"><?php echo _('Actions'); ?></th>
                    </tr>


<?php
/**
 * Check the database to see what tags we have
 */
$gettaglistq = "SELECT * FROM ".TBLPREFIX."tags WHERE tag_name > '' ORDER BY tag_sort_name ASC";
$gettaglistquery = mysqli_query($dbconn,$gettaglistq);
while ($gettaglistopt = mysqli_fetch_assoc($gettaglistquery)) {
    $tagid       = $gettaglistopt['tag_id'];
    $tagname     = retext($gettaglistopt['tag_name']);
    $tagslug     = $gettaglistopt['tag_slug'];
    $tagsort     = retext($gettaglistopt['tag_sort_name']);
    $tagdesc     = retext($gettaglistopt['tag_description']);
    $tagavtr     = $gettaglistopt['tag_avatar_url'];

    echo "\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."tag/".$tagslug."\"><img src=\"".$website_url.$tagavtr."\" class=\"dash-avatar\"></a>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."tag/".$tagslug."\">".$tagname."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t".$tagdesc."\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/edit-tag.php?tagid=".$tagid."\">"._('Edit')."</a>\n";
    echo "\t\t\t\t\t\t\t&nbsp;|&nbsp;\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/delete-tag.php?tagid=".$tagid."\">"._('Delete')."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t</tr>\n";
}
?>
                </table>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
