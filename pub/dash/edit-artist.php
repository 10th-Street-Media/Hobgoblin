<?php
/*
 * pub/dash/edit-artist.php
 *
 * Allows users to edit an artist.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

if (isset($_GET["aid"])) {
    $sel_id = $_GET["aid"];
} else {
    $sel_id = "";
}

if ($sel_id != '') {

    $getartistq = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_id='".$sel_id."'";
    $getartistquery = mysqli_query($dbconn,$getartistq);
    while ($getartistopt = mysqli_fetch_assoc($getartistquery)) {
        $artistid           = $getartistopt['artist_id'];
        $artistname         = $getartistopt['artist_name'];
        $artistslug         = $getartistopt['artist_slug'];
        $artistsort         = $getartistopt['artist_sort_name'];
        $artistavtr         = $getartistopt['artist_avatar_url'];
        $artistdesc         = $getartistopt['artist_description'];
        $artistrate         = $getartistopt['artist_rating'];
        $artistdob          = $getartistopt['artist_date_of_birth'];
        $artistdod          = $getartistopt['artist_date_of_death'];
        $artistnat          = $getartistopt['artist_nationality'];
        $artistpob          = $getartistopt['artist_place_of_birth'];
        $artistpod          = $getartistopt['artist_place_of_death'];
        $artistcod          = $getartistopt['artist_cause_of_death'];
        $artistoccu         = $getartistopt['artist_occupation'];
        $artistasso         = $getartistopt['artist_associates'];
    }

    // set the page title based on the title of the artist file
    $pagetitle = _("Edit $artistname « $website_name « ɧobgoblin");
}


/**
 * Form processing
 */
if (isset($_POST['art-submit'])) {
    $id         = $_POST['art-id'];
    $name       = nicetext($_POST['art-name']);
    $namesort   = nicetext($_POST['art-name-sort']);
    $thumburl   = trim(parse_url($_POST['art-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['art-desc']);
    $rate       = nicetext($_POST['art-rate']);
    $dob        = $_POST['art-dob'];
    $dod        = $_POST['art-dod'];
    $nat        = nicetext($_POST['art-nat']);
    $pob        = nicetext($_POST['art-pob']);
    $pod        = nicetext($_POST['art-pod']);
    $cod        = nicetext($_POST['art-cod']);
    $occu       = nicetext($_POST['art-occu']);
    $asso       = nicetext($_POST['art-asso']);

    if ($_POST['art-slug'] !== '') {
        $newslug    = nicetext($_POST['art-slug']);
    } else {
        $slug       = makeslug($_POST['art-name']);


        $artistslugqq = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_slug='".$slug."'";
        $artistslugquery = mysqli_query($dbconn,$artistslugqq);
        $artistslugrows = mysqli_num_rows($artistslugquery);
        $dupe = 2;

        if ($artistslugrows === 1) {

            $artistslugqq2 = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_slug LIKE '".$slug."-%'";
            $artistslugquery2 = mysqli_query($dbconn,$artistslugqq2);

            // Get the number of rows
            $artistslugrows2 = mysqli_num_rows($artistslugquery2);

            // add the number of rows to $dupe
            $newdupe = $dupe + $artistslugrows2;

            $newslug = $slug."-".$newdupe;
        } else {
            $newslug = $slug;
        }
    }


    // update the artist table
    $updartistq   = "UPDATE ".TBLPREFIX."artists SET artist_name='".$name."', artist_slug='".$newslug."', artist_description='".$desc."', artist_sort_name='".$namesort."', artist_avatar_url='".$thumburl."', artist_rating='".$rate."', artist_date_of_birth='".$dob."', artist_date_of_death='".$dod."', artist_nationality='".$nat."', artist_place_of_birth='".$pob."', artist_place_of_death='".$pod."', artist_cause_of_death='".$cod."', artist_occupation='".$occu."', artist_associates='".$asso."' WHERE artist_id='".$id."'";
    $updartistquery = mysqli_query($dbconn,$updartistq);


    // When we're done here, redirect to the artist's page
    redirect($website_url."the-artist.php?name=".$newslug);
}


include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">
<?php echo $updartistq; ?>
                <h2 class="w3-padding"><?php echo _("Edit artist"); ?></h2>
                <form method="post" action="edit-artist.php">
                    <input type="hidden" name="art-id" id="art-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="art-slug" id="art-slug" value="<?php echo $artistslug; ?>">
                    <input type="text" name="art-name" id="art-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistname; ?>" maxlength="255" required><br>
<?php
if ($artistavtr !== '') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$artistavtr."\" alt=\"".$artistname."\" title=\"".$artistname."\" class=\"w3-container w3-image\"><br><br>\n";
    echo "\t\t\t\t\t<label for=\"art-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"art-thumb-url\" id=\"art-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$artistavtr."\"><br>\n";
} else {
    echo "\t\t\t\t\t<label for=\"art-thumb-url\" class=\"w3-margin-left\">"._('Change avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"art-thumb-url\" id=\"art-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$artistavtr."\"><br>\n";
}
?>
                    <label for="art-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="art-name-sort" id="art-name-sort" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistsort; ?>"><br>
                    <label for="art-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="art-desc" id="summernote" class="w3-padding w3-margin-left"><?php echo $artistdesc; ?></textarea><br>
                    <label for="art-dob" class="w3-margin-left"><?php echo _('Date of birth'); ?></label>
                    <input type="date" name="art-dob" id="art-dob" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistdob; ?>"><br>
                    <label for="art-dod" class="w3-margin-left"><?php echo _('Date of death'); ?></label>
                    <input type="date" name="art-dod" id="art-dod" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistdod; ?>"><br>
                    <label for="art-pob" class="w3-margin-left"><?php echo _('Place of birth'); ?></label>
                    <input type="text" name="art-pob" id="art-pob" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistpob; ?>"><br>
                    <label for="art-pod" class="w3-margin-left"><?php echo _('Place of death'); ?></label>
                    <input type="text" name="art-pod" id="art-pod" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistpod; ?>"><br>
                    <label for="art-cod" class="w3-margin-left"><?php echo _('Cause of death'); ?></label>
                    <input type="text" name="art-cod" id="art-cod" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistcod; ?>"><br>
                    <label for="art-rate" class="w3-margin-left"><?php echo _('Rating'); ?></label>
                    <input type="text" name="art-rate" id="art-rate" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistrate; ?>" maxlength="2"><br>
                    <label for="art-nat" class="w3-margin-left"><?php echo _('Nationality'); ?></label>
                    <input type="text" name="art-nat" id="art-nat" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistnat; ?>"><br>
                    <label for="art-occu" class="w3-margin-left"><?php echo _('Occupation'); ?></label>
                    <input type="text" name="art-occu" id="art-occu" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistoccu; ?>"><br>
                    <label for="art-asso" class="w3-margin-left"><?php echo _('Associates'); ?></label>
                    <input type="text" name="art-asso" id="art-asso" class="w3-input w3-padding w3-margin-left" value="<?php echo $artistasso; ?>"><br>
                    <input type="submit" name="art-submit" id="art-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE ARTIST'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
