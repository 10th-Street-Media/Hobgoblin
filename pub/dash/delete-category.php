<?php
/*
 * pub/dash/delete-category.php
 *
 * A page where an category can be deleted.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../conn.php";
include			"../../functions.php";
require			"../includes/database-connect.php";
require_once	"../includes/configuration-data.php";
require_once	"../includes/verify-cookies.php";

if (isset($_GET["catid"])) {
	$sel_id = $_GET["catid"];
} else {
	$sel_id = "";
}

/**
 * Form processing
 */
if (isset($_POST['categorydelete'])) {

	$id		= $_POST['category-id'];

	$categoryq	= "DELETE FROM ".TBLPREFIX."categories WHERE category_id='".$id."'";
	$categoryquery = mysqli_query($dbconn,$categoryq);
	redirect($website_url."dash/categories.php");

} else if (isset($_POST['categorycancel'])) {
	redirect($website_url."dash/categories.php");
}


$pagetitle = _("Delete category « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete category"); ?></h2>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete this category?"); ?></b></p>
				<form method="post" action="delete-category.php">
					<input type="hidden" name="category-id" id="category-id" value="<?php echo $sel_id; ?>">
					<table>
						<tr>
							<td><input type="submit" name="categorydelete" id="categorydelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="categorycancel" id="categorycancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
