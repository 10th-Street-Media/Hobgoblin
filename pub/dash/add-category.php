<?php
/*
 * pub/dash/add-category.php
 *
 * Allows users to add a category.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";


/**
 * Form processing
 */
if (isset($_POST['cat-submit'])) {
    $name       = nicetext($_POST['cat-name']);
    $slug       = makeslug($_POST['cat-name']);
    $namesort   = nicetext($_POST['cat-name-sort']);
    $thumburl   = trim(parse_url($_POST['cat-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['cat-desc']);

    // add the category into the DB
    $addcategoryq   = "INSERT INTO ".TBLPREFIX."categories (category_id, category_name, category_slug, category_sort_name, category_avatar_url, category_description) VALUES ('', '".$name."', '".$slug."', '".$namesort."', '".$thumburl."', '".$desc."')";
    $addcategoryquery = mysqli_query($dbconn,$addcategoryq);


    // When we're done here, redirect to the category's page
    redirect($website_url."the-category.php?name=".$slug);
}

$pagetitle = _("Add a category « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Add a category"); ?></h2>
                <form method="post" action="add-category.php">
                    <label for="cat-name" class="w3-margin-left"><?php echo _('Category name'); ?></label>
                    <input type="text" name="cat-name" id="cat-name" class="w3-input w3-padding w3-margin-left" maxlength="255" required><br>
                    <label for="cat-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="cat-name-sort" id="cat-name-sort" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="cat-thumb-url" class="w3-margin-left"><?php echo _('Avatar URL'); ?></label>
                    <input type="text" name="cat-thumb-url" id="cat-thumb-url" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="cat-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="cat-desc" id="summernote" class="w3-padding w3-margin-left"></textarea><br>
                    <input type="submit" name="cat-submit" id="cat-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('ADD CATEGORY'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
