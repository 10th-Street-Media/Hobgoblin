<?php
/*
 * pub/dash/companies.php
 *
 * A page listing all companies that create or distribute media on this instance.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

$pagetitle = _("Companies « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Companies"); ?></h2>

                <a href="add-company.php" class="w3-button w3-button-hover w3-theme-d3 w3-margin-left"><?php echo _("Add a company"); ?></a><br><br>

                <table class="w3-table-all w3-hoverable w3-margin-left">
                    <tr class="w3-theme-dark">
                        <th class="w3-center"><?php echo _('Company'); ?></th>
                        <th class="w3-center"><?php echo _('Description'); ?></th>
                        <th class="w3-center"><?php echo _('Actions'); ?></th>
                    </tr>


<?php
/**
 * Check the database to see what companies we have
 */
$getcompanylistq = "SELECT * FROM ".TBLPREFIX."companies WHERE company_name > '' ORDER BY company_sort_name ASC";
$getcompanylistquery = mysqli_query($dbconn,$getcompanylistq);
while ($getcompanylistopt = mysqli_fetch_assoc($getcompanylistquery)) {
    $companyid       = $getcompanylistopt['company_id'];
    $companyname     = $getcompanylistopt['company_name'];
    $companyslug     = $getcompanylistopt['company_slug'];
    $companysort     = $getcompanylistopt['company_sort_name'];
    $companydesc     = $getcompanylistopt['company_description'];
    $companyavtr     = urldecode($getcompanylistopt['company_avatar_url']);

    echo "\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."company/".$companyslug."\"><img src=\"".$website_url.$companyavtr."\" class=\"dash-avatar\"></a>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."company/".$companyslug."\">".$companyname."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t".$companydesc."\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/edit-company.php?cid=".$companyid."\">"._('Edit')."</a>\n";
    echo "\t\t\t\t\t\t\t&nbsp;|&nbsp;\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/delete-company.php?cid=".$companyid."\">"._('Delete')."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t</tr>\n";
}
?>
                </table>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
