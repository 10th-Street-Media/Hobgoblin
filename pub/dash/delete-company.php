<?php
/*
 * pub/dash/delete-company.php
 *
 * A page where an company can be deleted.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../conn.php";
include			"../../functions.php";
require			"../includes/database-connect.php";
require_once	"../includes/configuration-data.php";
require_once	"../includes/verify-cookies.php";

if (isset($_GET["cid"])) {
	$sel_id = $_GET["cid"];
} else {
	$sel_id = "";
}

/**
 * Form processing
 */
if (isset($_POST['companydelete'])) {

	$id		= $_POST['company-id'];

	$companyq	= "DELETE FROM ".TBLPREFIX."companies WHERE company_id='".$id."'";
	$companyquery = mysqli_query($dbconn,$companyq);
	redirect($website_url."dash/companies.php");

} else if (isset($_POST['companycancel'])) {
	redirect($website_url."dash/companies.php");
}


$pagetitle = _("Delete company « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete company"); ?></h2>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete this company?"); ?></b></p>
				<form method="post" action="delete-company.php">
					<input type="hidden" name="company-id" id="company-id" value="<?php echo $sel_id; ?>">
					<table>
						<tr>
							<td><input type="submit" name="companydelete" id="companydelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="companycancel" id="companycancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
