<?php
/*
 * pub/dash/admin/suspend-user.php
 *
 * A page where an admin can suspend a user for a period of time.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../../conn.php";
include			"../../../functions.php";
require			"../../includes/database-connect.php";
require_once	"../../includes/configuration-data.php";
require_once	"../../includes/verify-cookies.php";

if (isset($_GET["uid"])) {
	$sel_id = $_GET["uid"];
} else {
	$sel_id = "";
}

/**
 * Get the username
 */
$usernameq  = "SELECT * FROM ".TBLPREFIX."users WHERE user_id='".$sel_id."'";
$usernamequery = mysqli_query($dbconn,$usernameq);
while ($usernameopt = mysqli_fetch_assoc($usernamequery)) {
    $uname = $usernameopt['user_name'];
}



/**
 * Form processing
 */
if (isset($_POST['usersusp'])) {

	$id			= $_POST['user-id'];
    $name   	= $_POST['user-name'];
    $susp       = $_POST['usersuspuntil'];
	$udatesusp	= date('Y-m-d H:i:s');

    /**
     * Suspend the user
     */
	$suspuserq	= "UPDATE ".TBLPREFIX."users SET user_suspended_until='".$susp."', user_suspended_on='".$udateban."', user_suspended_by='".$_COOKIE['uname']."' WHERE user_id='".$id."'";
	$suspuserquery = mysqli_query($dbconn,$suspuserq);


    redirect($website_url."dash/admin/users.php");
} else if (isset($_POST['usercancel'])) {
	redirect($website_url."dash/admin/users.php");
}



$pagetitle = _("Suspend a user « $website_name « ɧobgoblin");
include "header.php";
include "../nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Suspend $uname"); ?></h2>
                <p class="w3-padding"><?php echo _('Suspending a user will prevent them from logging in for a certain amount of time and will show them an error message when they try to log in.'); ?></p>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to suspend $uname?"); ?></b></p>
				<form method="post" action="suspend-user.php">
					<input type="hidden" name="user-id" id="user-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="user-name" id="user-name" value="<?php echo $uname; ?>">
					<p>
                        <label for="usersuspuntil"><?php echo _("Suspend until"); ?></label>
				        <input type="date" name="usersuspuntil" id="usersuspuntil" class="w3-input w3-border w3-margin-bottom" title="<?php echo _('Suspend the user until this date'); ?>">
                    </p>
                    <p>
				        <input type="submit" name="usersusp" id="usersusp" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>">&nbsp;
						<input type="submit" name="usercancel" id="usercancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>">
                    </p>
				</form>

			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
