<?php
/*
 * pub/dash/admin/delete-user.php
 *
 * A page where an admin can delete a user from this instance.
 * This is like the concept of "damnatio memoriae", where most
 * traces of the user are erased from this instance.
 *
 * It is an extreme step, and shouldn't be taken lightly.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../../conn.php";
include			"../../../functions.php";
require			"../../includes/database-connect.php";
require_once	"../../includes/configuration-data.php";
require_once	"../../includes/verify-cookies.php";

if (isset($_GET["uid"])) {
	$sel_id = $_GET["uid"];
} else {
	$sel_id = "";
}

/**
 * Get the username
 */
$usernameq  = "SELECT * FROM ".TBLPREFIX."users WHERE user_id='".$sel_id."'";
$usernamequery = mysqli_query($dbconn,$usernameq);
while ($usernameopt = mysqli_fetch_assoc($usernamequery)) {
    $uname = $usernameopt['user_name'];
}

/**
 * Form processing
 */
if (isset($_POST['userdelete'])) {

	$id		= $_POST['user-id'];
    $name   = $_POST['user-name'];

    /**
     * Delete the user from the users table
     * In this future, this should probably lead to a tombstone, not a 404
     */
	$userq	= "DELETE FROM ".TBLPREFIX."users WHERE user_id='".$id."'";
	$userquery = mysqli_query($dbconn,$userq);

    /**
     * Get the list of deleted user names
     * Add this user to that list
     * Update the list of deleted user names
     * In this future, this should probably lead to a tombstone, not a 404
     */
    $getdeletedusersq    = "SELECT * FROM ".TBLPREFIX."configuration";
    $getdeletedusersquery = mysqli_query($dbconn,$getdeletedusersq);
    while ($getdeletedusersopt = mysqli_fetch_assoc($getdeletedusersquery)) {
        $deletedusers       = preg_split('/,/i',$getdeletedusersopt['deleted_user_names']);
        $deletedusers[]     = $name;
        $deleteduserslist   = join(',',$deletedusers);

        $upddeletedusersq = "UPDATE ".TBLPREFIX."configuration SET deleted_user_names='".$deleteduserslist."'";
        $upddeletedusersquery = mysqli_query($dbconn,$upddeletedusersq);
    }

    /**
     * Remove any media from this user
     * In this future, this should probably lead to a tombstone, not a 404
     */
    $usermediaq	= "DELETE FROM ".TBLPREFIX."media WHERE user_name='".$name."'";
	$usermediaquery = mysqli_query($dbconn,$usermediaq);

    /**
     * Remove any posts and pages by this user
     * In this future, this should probably lead to a tombstone, not a 404
     */
    $userpostsq	= "DELETE FROM ".TBLPREFIX."posts WHERE user_id='".$id."'";
	$userpostsquery = mysqli_query($dbconn,$userpostsq);

    /**
     * Remove any messages by this user
     * In this future, this should probably lead to a tombstone, not a 404
     */
    $usermessagesq	= "DELETE FROM ".TBLPREFIX."messages WHERE user_name='".$name."'";
	$usermessagesquery = mysqli_query($dbconn,$usermessagesq);


    redirect($website_url."dash/admin/users.php");
} else if (isset($_POST['usercancel'])) {
	redirect($website_url."dash/admin/users.php");
}


$pagetitle = _("Delete a user « $website_name « ɧobgoblin");
include "header.php";
include "../nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete $uname"); ?></h2>
                <p class="w3-padding"><?php echo _('Deleting a user will remove their profile and their content from this instance only. Their username will be banned from being used again on this instance. If anyone reposted this user\'s content, it may exist elsewhere on the network.'); ?></p>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete $uname?"); ?></b></p>
				<form method="post" action="delete-user.php">
					<input type="hidden" name="user-id" id="user-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="user-name" id="user-name" value="<?php echo $uname; ?>">
					<table>
						<tr>
							<td><input type="submit" name="userdelete" id="userdelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="usercancel" id="usercancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>

			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
