<?php
/*
 * pub/dash/admin/edit-user.php
 *
 * A page where an admin can edit a user profile.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../../conn.php";
include			"../../../functions.php";
require			"../../includes/database-connect.php";
require_once	"../../includes/configuration-data.php";
require_once	"../../includes/verify-cookies.php";

if (isset($_GET["uid"])) {
	$sel_id = $_GET["uid"];
} else {
	$sel_id = "";
}

if ($sel_id != '') {

    $getuserq = "SELECT * FROM ".TBLPREFIX."users WHERE user_id='".$sel_id."'";
    $getuserquery = mysqli_query($dbconn,$getuserq);
    while ($getuseropt = mysqli_fetch_assoc($getuserquery)) {
        $uid        = $getuseropt['user_id'];
        $uname      = $getuseropt['user_name'];
        $udname     = $getuseropt['user_display_name'];
        $uemail     = $getuseropt['user_email'];
        $udob       = $getuseropt['user_date_of_birth'];
        $ulevel     = $getuseropt['user_level'];
        $uactor     = $getuseropt['user_actor_type'];
        $uavatar    = $getuseropt['user_avatar'];
        $ulocale    = $getuseropt['user_locale'];
        $uplace     = $getuseropt['user_locations'];
        $utzone     = $getuseropt['user_time_zone'];
        $ubio       = $getuseropt['user_bio'];
        $ususpto    = $getuseropt['user_suspended_until'];
        $ususpon    = $getuseropt['user_suspended_on'];
        $ususpby    = $getuseropt['user_suspended_by'];
        $uban       = $getuseropt['user_is_banned'];
        $ubanon     = $getuseropt['user_banned_on'];
        $ubanby     = $getuseropt['user_banned_by'];
        $ucreate    = $getuseropt['user_created'];
        $ulast      = $getuseropt['user_last_login'];
    }

}



/**  **************************************************************************
 *
 *   FORM PROCESSING
 *
 **  *************************************************************************/
if(isset($_POST['profsubmit'])) {

    /* Set our variables */
    $userid         = $_POST['user-id'];
    $username       = $_POST['user-name'];
    $userdname      = nicetext($_POST['user-dname']);
    $useremail      = $_POST['user-email'];
    $userdob        = $_POST['user-dob'];
    $userprvkey     = $_POST['user-prv-key'];
    $userpubkey     = $_POST['user-pub-key'];
    $userlocale     = $_POST['user-l10n'];
    $userlocation   = $_POST['user-loc'];
    $usertzone      = $_POST['user-tzone'];
    $userbio        = nicetext($_POST['user-bio']);

    /**
     * If we don't have public or private keys, let's make some
     * The private key gets written to ../../keys/username-private.pem
     * The public key gets written to the database
     */
    if ($userprvkey == "") {
        // from the comments on https://www.php.net/manual/en/function.openssl-pkey-new.php
        $keyconfig = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($keyconfig);

        // Extract the private key from $res to $privkey
        openssl_pkey_export($res, $privkey);

        // write the private key to a file outside the web root
        $privmeta = fopen("../../keys/".$username."-private.pem", "w") or die("Unable to open or create ../../keys/".$username."-private.pem file");
        fwrite($privmeta,$privkey);

        // Extract the public key from $res to $pubkey
        $pubkey = openssl_pkey_get_details($res);
        $pubkey = $pubkey["key"];

    }

    $userupdq = "UPDATE ".TBLPREFIX."users SET user_display_name='".$userdname."', user_email='".$useremail."', user_date_of_birth='".$userdob."', user_pub_key='".$pubkey."', user_locale='".$userlocale."', user_location='".$userlocation."', user_time_zone='".$usertzone."', user_bio='".$userbio."' WHERE user_name='".$_COOKIE['uname']."'";
    $userupdquery = mysqli_query($dbconn,$userupdq);

    /* reload the page by redirecting to itself */
    #redirect($website_url."dash/profile.php");

} /* end if isset $_POST['profsubmit'] */
/**  END FORM PROCESSING  ****************************************************/


$pagetitle = _("Edit a user « $website_name « ɧobgoblin");
include "header.php";
include "../nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Edit user"); ?></h2>
                <form method="post" action="edit-user.php">
                    <input type="hidden" name="user-id" id="user-id" value="<?php echo $sel_id; ?>">
                    <label for="user-name" class="w3-margin-left"><?php echo _('Username'); ?></label>
                    <input type="text" name="user-name" id="user-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $uname; ?>" maxlength="20" required><br>
                    <label for="user-display-name" class="w3-margin-left"><?php echo _('Display name'); ?></label>
                    <input type="text" name="user-name" id="user-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $udname; ?>" maxlength="255" required><br>
                    <?php
if ($uavatar !== '') {
    echo "\t\t\t\t\t<img src=\"".$uavatar."\" alt=\"".$uname."\" title=\"".$uname."\" class=\"w3-container w3-image\"><br><br>\n";
} else {
    echo "\t\t\t\t\t<label for=\"user-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"user-thumb-url\" id=\"user-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$uavatar."\"><br>\n";
}
?>
                    <label for="user-email" class="w3-margin-left"><?php echo _('Email address'); ?></label>
                    <input type="email" name="user-email" id="user-email" class="w3-input w3-padding w3-margin-left" value="<?php echo $uemail; ?>" required><br>
                    <label for="user-dob" class="w3-margin-left"><?php echo _('Date of birth'); ?></label>
                    <input type="date" name="user-dob" id="user-dob" class="w3-input w3-padding w3-margin-left" min="1900-01-01" value="<?php echo $udob; ?>" required><br>
                    <label for="user-level" class="w3-margin-left"><?php echo _('Level'); ?></label>
                    <select name="user-level" id="user-level" class="w3-input w3-padding w3-margin-left">
<?php
// get the list of user levels from the DB
$levelq = "SELECT * FROM ".TBLPREFIX."user_levels ORDER BY user_level_name ASC";
$levelquery = mysqli_query($dbconn,$levelq);
while ($levelopt = mysqli_fetch_assoc($levelquery)) {
    if ($ulevel === $levelopt['user_level_name']) {
        echo "\t\t\t\t\t\t<option value=\"".$levelopt['user_level_name']."\" selected>".$levelopt['user_level_name']."</option>\n";
    } else {
        echo "\t\t\t\t\t\t<option value=\"".$levelopt['user_level_name']."\">".$levelopt['user_level_name']."</option>\n";
    }
}
?>
                    </select><br>
                    <label for="user-type" class="w3-margin-left"><?php echo _('Type'); ?></label>
                    <select name="user-type" id="user-type" class="w3-input w3-padding w3-margin-left">
<?php
// get the list of actor types from the DB
$typeq = "SELECT * FROM ".TBLPREFIX."actor_types ORDER BY actor_type_name ASC";
$typequery = mysqli_query($dbconn,$typeq);
while ($typeopt = mysqli_fetch_assoc($typequery)) {
    if ($uactor === $typeopt['actor_type_name']) {
        echo "\t\t\t\t\t\t<option value=\"".$typeopt['actor_type_name']."\" selected>".$typeopt['actor_type_name']."</option>\n";
    } else {
        echo "\t\t\t\t\t\t<option value=\"".$typeopt['actor_type_name']."\">".$typeopt['actor_type_name']."</option>\n";
    }
}
?>
                    </select><br>
                    <label for="user-l10n" class="w3-margin-left"><?php echo _('Locale'); ?></label>
                    <select name="user-l10n" id="user-l10n" class="w3-input w3-padding w3-margin-left">
<?php
// get the list of locales from the DB
$l10nq = "SELECT * FROM ".TBLPREFIX."locales ORDER BY locale_language, locale_country ASC";
$l10nquery = mysqli_query($dbconn,$l10nq);
while ($l10nopt = mysqli_fetch_assoc($l10nquery)) {
    if ($ulocale === $l10nopt['locale_language']."-".$l10nopt['locale_country']) {
        echo "\t\t\t\t\t\t<option value=\"".$l10nopt['locale_language']."-".$l10nopt['locale_country']."\" selected>".$l10nopt['locale_language']."-".$l10nopt['locale_country']."</option>\n";
    } else {
        echo "\t\t\t\t\t\t<option value=\"".$l10nopt['locale_language']."-".$l10nopt['locale_country']."\">".$l10nopt['locale_language']."-".$l10nopt['locale_country']."</option>\n";
    }
}
?>
                    </select><br>
                    <label for="user-loc" class="w3-margin-left"><?php echo _('Location'); ?></label>
                    <select name="user-loc" id="user-loc" class="w3-input w3-padding w3-margin-left">
<?php
// get the list of locations from the DB
$locq = "SELECT * FROM ".TBLPREFIX."locations ORDER BY location_name ASC";
$locquery = mysqli_query($dbconn,$locq);
while ($locopt = mysqli_fetch_assoc($locquery)) {
    if ($uplace === $locopt['location_id']) {
        echo "\t\t\t\t\t\t<option value=\"".$locopt['location_id']."\" selected>".$locopt['location_name']."</option>\n";
    } else {
        echo "\t\t\t\t\t\t<option value=\"".$locopt['location_id']."\">".$locopt['location_name']."</option>\n";
    }
}
?>
                    </select><br>
                    <label for="user-tzone" class="w3-margin-left"><?php echo _('Time zone'); ?></label>
                    <select name="user-tzone" id="user-tzone" class="w3-input w3-padding w3-margin-left">
<?php
// get the list of time zones from the DB
$tzq = "SELECT * FROM ".TBLPREFIX."time_zones ORDER BY time_zone_name ASC";
$tzquery = mysqli_query($dbconn,$tzq);
while ($tzopt = mysqli_fetch_assoc($tzquery)) {
    if ($utzone === $tzopt['time_zone_name']) {
        echo "\t\t\t\t\t\t<option value=\"".$tzopt['time_zone_name']."\" selected>".$tzopt['time_zone_name']."</option>\n";
    } else {
        echo "\t\t\t\t\t\t<option value=\"".$tzopt['time_zone_name']."\">".$tzopt['time_zone_name']."</option>\n";
    }
}
?>
                    </select><br>
                    <label for="user-bio" class="w3-margin-left"><?php echo _('Biography'); ?></label>
                    <textarea name="user-bio" id="summernote" class="w3-padding w3-margin-left"><?php echo $ubio; ?></textarea><br>
                    <input type="submit" name="user-submit" id="user-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE USER'); ?>">
                </form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>
<?php
include "footer.php";
?>
