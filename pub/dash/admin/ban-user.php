<?php
/*
 * pub/dash/admin/ban-user.php
 *
 * A page where an admin can ban a user.
 * Banning is like a permanent suspension.
 * The user will get an error if they try to login,
 * but their content will not be deleted en masse.
 * The content can still be deleted individually.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../../conn.php";
include			"../../../functions.php";
require			"../../includes/database-connect.php";
require_once	"../../includes/configuration-data.php";
require_once	"../../includes/verify-cookies.php";

if (isset($_GET["uid"])) {
	$sel_id = $_GET["uid"];
} else {
	$sel_id = "";
}

/**
 * Get the username
 */
$usernameq  = "SELECT * FROM ".TBLPREFIX."users WHERE user_id='".$sel_id."'";
$usernamequery = mysqli_query($dbconn,$usernameq);
while ($usernameopt = mysqli_fetch_assoc($usernamequery)) {
    $uname = $usernameopt['user_name'];
}


/**
 * Form processing
 */
if (isset($_POST['userban'])) {

	$id			= $_POST['user-id'];
    $name   	= $_POST['user-name'];
	$udateban	= date('Y-m-d H:i:s');

    /**
     * Ban the user
     */
	$banuserq	= "UPDATE ".TBLPREFIX."users SET user_is_banned='1', user_banned_on='".$udateban."', user_banned_by='".$_COOKIE['uname']."' WHERE user_id='".$id."'";
	$banuserquery = mysqli_query($dbconn,$banuserq);

    /**
     * Get the list of banned user names
     * Add this user to that list
     * Update the list of banned user names
     * In this future, this should probably lead to a tombstone, not a 404
     */
    $getbannedusersq    = "SELECT * FROM ".TBLPREFIX."configuration";
    $getbannedusersquery = mysqli_query($dbconn,$getbannedusersq);
    while ($getbannedusersopt = mysqli_fetch_assoc($getbannedusersquery)) {
        $bannedusers       = preg_split('/,/i',$getbannedusersopt['banned_user_names']);
        $bannedusers[]     = $name;
        $banneduserslist   = join(',',$bannedusers);

        $updbannedusersq = "UPDATE ".TBLPREFIX."configuration SET banned_user_names='".$banneduserslist."'";
        $updbannedusersquery = mysqli_query($dbconn,$updbannedusersq);
    }


    redirect($website_url."dash/admin/users.php");
} else if (isset($_POST['usercancel'])) {
	redirect($website_url."dash/admin/users.php");
}


$pagetitle = _("Ban a user « $website_name « ɧobgoblin");
include "header.php";
include "../nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Ban $uname"); ?></h2>
                <p class="w3-padding"><?php echo _('Banning a user will prevent them from logging in and will show them an error message.'); ?></p>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to ban $uname?"); ?></b></p>
				<form method="post" action="ban-user.php">
					<input type="hidden" name="user-id" id="user-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="user-name" id="user-name" value="<?php echo $uname; ?>">
					<table>
						<tr>
							<td><input type="submit" name="userban" id="userban" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="usercancel" id="usercancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>

			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
