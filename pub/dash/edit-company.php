<?php
/*
 * pub/dash/edit-company.php
 *
 * Allows users to edit a company.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

if (isset($_GET["cid"])) {
    $sel_id = $_GET["cid"];
} else {
    $sel_id = "";
}

if ($sel_id != '') {

    $getcompanyq = "SELECT * FROM ".TBLPREFIX."companies WHERE company_id='".$sel_id."'";
    $getcompanyquery = mysqli_query($dbconn,$getcompanyq);
    while ($getcompanyopt = mysqli_fetch_assoc($getcompanyquery)) {
        $companyid          = $getcompanyopt['company_id'];
        $companyname        = $getcompanyopt['company_name'];
        $companyslug        = $getcompanyopt['company_slug'];
        $companysort        = $getcompanyopt['company_sort_name'];
        $companyavtr        = $getcompanyopt['company_avatar_url'];
        $companydesc        = $getcompanyopt['company_description'];
        $companyrate        = $getcompanyopt['company_rating'];
        $companystart       = $getcompanyopt['company_start_year'];
        $companyend         = $getcompanyopt['company_end_year'];
        $companyctry        = $getcompanyopt['company_country'];
        $companyindu        = $getcompanyopt['company_industries'];
        $companyasso        = $getcompanyopt['company_associates'];
    }

    // set the page title based on the title of the company file
    $pagetitle = _("Edit $companyname « $website_name « ɧobgoblin");
}


/**
 * Form processing
 */
if (isset($_POST['comp-submit'])) {
    $id         = $_POST['comp-id'];
    $name       = nicetext($_POST['comp-name']);
    $namesort   = nicetext($_POST['comp-name-sort']);
    $thumburl   = trim(parse_url($_POST['comp-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['comp-desc']);
    $rate       = nicetext($_POST['comp-rate']);
    $start      = $_POST['comp-start'];
    $end        = $_POST['comp-end'];
    $ctry       = nicetext($_POST['comp-ctry']);
    $indu       = nicetext($_POST['comp-indu']);
    $asso       = nicetext($_POST['comp-asso']);

    if ($_POST['comp-slug'] !== '') {
        $newslug    = nicetext($_POST['comp-slug']);
    } else {
        $slug       = makeslug($_POST['comp-name']);


        $companyslugqq = "SELECT * FROM ".TBLPREFIX."companies WHERE company_slug='".$slug."'";
        $companyslugquery = mysqli_query($dbconn,$companyslugqq);
        $companyslugrows = mysqli_num_rows($companyslugquery);
        $dupe = 2;

        if ($companyslugrows === 1) {

            $companyslugqq2 = "SELECT * FROM ".TBLPREFIX."companies WHERE company_slug LIKE '".$slug."-%'";
            $companyslugquery2 = mysqli_query($dbconn,$companyslugqq2);

            // Get the number of rows
            $companyslugrows2 = mysqli_num_rows($companyslugquery2);

            // add the number of rows to $dupe
            $newdupe = $dupe + $companyslugrows2;

            $newslug = $slug."-".$newdupe;
        } else {
            $newslug = $slug;
        }
    }



    // update the company table
    $updcompanyq   = "UPDATE ".TBLPREFIX."companies SET company_name='".$name."', company_slug='".$newslug."', company_description='".$desc."', company_sort_name='".$namesort."', company_avatar_url='".$thumburl."', company_rating='".$rate."', company_start_year='".$start."', company_end_year='".$end."', company_country='".$ctry."', company_industries='".$indu."', company_associates='".$asso."' WHERE company_id='".$id."'";
    $updcompanyquery = mysqli_query($dbconn,$updcompanyq);


    // When we're done here, redirect to the company's page
    redirect($website_url."the-company.php?name=".$newslug);
}


include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">
<?php echo $updcompanyq; ?>
                <h2 class="w3-padding"><?php echo _("Edit company"); ?></h2>
                <form method="post" action="edit-company.php">
                    <input type="hidden" name="comp-id" id="comp-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="comp-slug" id="comp-slug" value="<?php echo $companyslug; ?>">
                    <input type="text" name="comp-name" id="comp-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $companyname; ?>" maxlength="255" required><br>
<?php
if ($companyavtr !== '') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$companyavtr."\" alt=\"".$companyname."\" title=\"".$companyname."\" class=\"w3-container w3-image\"><br><br>\n";
    echo "\t\t\t\t\t<label for=\"comp-thumb-url\" class=\"w3-margin-left\">"._('Change avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"comp-thumb-url\" id=\"comp-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$companyavtr."\"><br>\n";
} else {
    echo "\t\t\t\t\t<label for=\"comp-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"comp-thumb-url\" id=\"comp-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$companyavtr."\"><br>\n";
}
?>
                    <label for="comp-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="comp-name-sort" id="comp-name-sort" class="w3-input w3-padding w3-margin-left" value="<?php echo $companysort; ?>"><br>
                    <label for="comp-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="comp-desc" id="summernote" class="w3-padding w3-margin-left"><?php echo $companydesc; ?></textarea><br>
                    <label for="comp-start" class="w3-margin-left"><?php echo _('Start year'); ?></label>
                    <input type="year" name="comp-start" id="comp-start" class="w3-input w3-padding w3-margin-left" value="<?php echo $companystart; ?>"><br>
                    <label for="comp-end" class="w3-margin-left"><?php echo _('End year'); ?></label>
                    <input type="year" name="comp-end" id="comp-end" class="w3-input w3-padding w3-margin-left" value="<?php echo $companyend; ?>"><br>
                    <label for="comp-ctry" class="w3-margin-left"><?php echo _('Country'); ?></label>
                    <input type="text" name="comp-ctry" id="comp-ctry" class="w3-input w3-padding w3-margin-left" value="<?php echo $companyctry; ?>"><br>
                    <label for="comp-indu" class="w3-margin-left"><?php echo _('Industries'); ?></label>
                    <input type="text" name="comp-indu" id="comp-indu" class="w3-input w3-padding w3-margin-left" value="<?php echo $companyindu; ?>"><br>
                    <label for="comp-asso" class="w3-margin-left"><?php echo _('Associates'); ?></label>
                    <input type="text" name="comp-asso" id="comp-asso" class="w3-input w3-padding w3-margin-left" value="<?php echo $companyasso; ?>"><br>
                    <input type="submit" name="comp-submit" id="comp-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE COMPANY'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
