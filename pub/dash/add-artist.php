<?php
/*
 * pub/dash/add-artist.php
 *
 * Allows users to add an artist.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";


/**
 * Form processing
 */
if (isset($_POST['art-submit'])) {
    $name       = nicetext($_POST['art-name']);
    $slug       = makeslug($_POST['art-name']);
    $namesort   = nicetext($_POST['art-name-sort']);
    $thumburl   = trim(parse_url($_POST['art-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['art-desc']);
    $rate       = nicetext($_POST['art-rate']);
    $dob        = $_POST['art-dob'];
    $dod        = $_POST['art-dod'];
    $nat        = nicetext($_POST['art-nat']);
    $pob        = nicetext($_POST['art-pob']);
    $pod        = nicetext($_POST['art-pod']);
    $cod        = nicetext($_POST['art-cod']);
    $occu       = nicetext($_POST['art-occu']);
    $asso       = nicetext($_POST['art-asso']);

    // Let's double check the slug and see if it's unique
    $artistslugqq = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_slug='".$slug."'";
    $artistslugquery = mysqli_query($dbconn,$artistslugqq);
    $artistslugrows = mysqli_num_rows($artistslugquery);
    $dupe = 2;

    if ($artistslugrows === 1) {

        $artistslugqq2 = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_slug LIKE '".$slug."-%'";
        $artistslugquery2 = mysqli_query($dbconn,$artistslugqq2);

        // Get the number of rows
        $artistslugrows2 = mysqli_num_rows($artistslugquery2);

        // add the number of rows to $dupe
        $newdupe = $dupe + $artistslugrows2;

        $newslug = $slug."-".$newdupe;

    } else {
        $newslug = $slug;
    }

    // add the artist into the DB
    $addartistq   = "INSERT INTO ".TBLPREFIX."artists (artist_id, artist_name, artist_slug, artist_sort_name, artist_avatar_url, artist_description, artist_rating, artist_date_of_birth, artist_date_of_death, artist_nationality, artist_place_of_birth, artist_place_of_death, artist_cause_of_death, artist_occupation, artist_associates) VALUES ('', '".$name."', '".$newslug."', '".$namesort."', '".$thumburl."', '".$desc."', '".$rate."', '".$dob."', '".$dod."', '".$nat."', '".$pob."', '".$pod."', '".$cod."', '".$occu."', '".$asso."')";
    $addartistquery = mysqli_query($dbconn,$addartistq);


    // When we're done here, redirect to the artist's page
    redirect($website_url."the-artist.php?name=".$newslug);
}

$pagetitle = _("Add an artist « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Add an artist"); ?></h2>
                <form method="post" action="add-artist.php">
                    <label for="art-name" class="w3-margin-left"><?php echo _('Artist name'); ?></label>
                    <input type="text" name="art-name" id="art-name" class="w3-input w3-padding w3-margin-left" maxlength="255" required><br>
                    <label for="art-thumb-url" class="w3-margin-left"><?php echo _('Avatar URL'); ?></label>
                    <input type="text" name="art-thumb-url" id="art-thumb-url" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="art-name-sort" id="art-name-sort" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="art-desc" id="summernote" class="w3-padding w3-margin-left"></textarea><br>
                    <label for="art-dob" class="w3-margin-left"><?php echo _('Date of birth'); ?></label>
                    <input type="date" name="art-dob" id="art-dob" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-dod" class="w3-margin-left"><?php echo _('Date of death'); ?></label>
                    <input type="date" name="art-dod" id="art-dod" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-pob" class="w3-margin-left"><?php echo _('Place of birth'); ?></label>
                    <input type="text" name="art-pob" id="art-pob" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-pod" class="w3-margin-left"><?php echo _('Place of death'); ?></label>
                    <input type="text" name="art-pod" id="art-pod" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-cod" class="w3-margin-left"><?php echo _('Cause of death'); ?></label>
                    <input type="text" name="art-cod" id="art-cod" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-rate" class="w3-margin-left"><?php echo _('Rating'); ?></label>
                    <input type="text" name="art-rate" id="art-rate" class="w3-input w3-padding w3-margin-left" maxlength="2"><br>
                    <label for="art-nat" class="w3-margin-left"><?php echo _('Nationality'); ?></label>
                    <input type="text" name="art-nat" id="art-nat" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-occu" class="w3-margin-left"><?php echo _('Occupation'); ?></label>
                    <input type="text" name="art-occu" id="art-occu" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="art-asso" class="w3-margin-left"><?php echo _('Associates'); ?></label>
                    <input type="text" name="art-asso" id="art-asso" class="w3-input w3-padding w3-margin-left"><br>
                    <input type="submit" name="art-submit" id="art-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('ADD ARTIST'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
