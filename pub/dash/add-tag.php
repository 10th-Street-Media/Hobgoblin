<?php
/*
 * pub/dash/add-tag.php
 *
 * Allows users to add a tag.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";


/**
 * Form processing
 */
if (isset($_POST['tag-submit'])) {
    $name       = nicetext($_POST['tag-name']);
    $slug       = makeslug($_POST['tag-name']);
    $namesort   = nicetext($_POST['tag-name-sort']);
    $thumburl   = trim(parse_url($_POST['tag-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['tag-desc']);

    // add the tag into the DB
    $addtagq   = "INSERT INTO ".TBLPREFIX."tags (tag_id, tag_name, tag_slug, tag_sort_name, tag_avatar_url, tag_description) VALUES ('', '".$name."', '".$slug."', '".$namesort."', '".$thumburl."', '".$desc."')";
    $addtagquery = mysqli_query($dbconn,$addtagq);


    // When we're done here, redirect to the tag's page
    redirect($website_url."the-tag.php?name=".$slug);
}

$pagetitle = _("Add a tag « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Add a tag"); ?></h2>
                <form method="post" action="add-tag.php">
                    <label for="tag-name" class="w3-margin-left"><?php echo _('Tag name'); ?></label>
                    <input type="text" name="tag-name" id="tag-name" class="w3-input w3-padding w3-margin-left" maxlength="255" required><br>
                    <label for="tag-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="tag-name-sort" id="tag-name-sort" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="tag-thumb-url" class="w3-margin-left"><?php echo _('Avatar URL'); ?></label>
                    <input type="text" name="tag-thumb-url" id="tag-thumb-url" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="tag-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="tag-desc" id="summernote" class="w3-padding w3-margin-left"></textarea><br>
                    <input type="submit" name="tag-submit" id="tag-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('ADD CATEGORY'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
