<?php
/*
 * pub/dash/add-collection.php
 *
 * Allows users to add an collection.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";


/**
 * Form processing
 */
if (isset($_POST['coll-submit'])) {
    $name       = nicetext($_POST['coll-name']);
    $slug       = makeslug($_POST['coll-name']);
    $namesort   = nicetext($_POST['coll-name-sort']);
    $thumburl   = trim(parse_url($_POST['coll-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['coll-desc']);
    $rate       = nicetext($_POST['coll-rate']);
    $art        = nicetext($_POST['coll-art']);
    $comp       = nicetext($_POST['coll-comp']);
    $year       = $_POST['coll-year'];


    // add the collection into the DB
    $addcollectionq   = "INSERT INTO ".TBLPREFIX."collections (collection_id, collection_name, collection_slug, collection_sort_name, collection_avatar_url, collection_description, collection_rating, collection_artist, collection_company, collection_year, collection_date) VALUES ('', '".$name."', '".$slug."', '".$namesort."', '".$thumburl."', '".$desc."', '".$rate."', '".$art."', '".$comp."', '".$year."', '".date('Y-m-d H:i:s')."')";
    $addcollectionquery = mysqli_query($dbconn,$addcollectionq);


    // When we're done here, redirect to the artist's page
    redirect($website_url."the-collection.php?name=".$slug);
}

$pagetitle = _("Add a collection « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Add a collection"); ?></h2>
                <form method="post" action="add-collection.php">
                    <label for="coll-name" class="w3-margin-left"><?php echo _('Collection name'); ?></label>
                    <input type="text" name="coll-name" id="coll-name" class="w3-input w3-padding w3-margin-left" maxlength="255" required><br>
                    <label for="coll-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="coll-name-sort" id="coll-name-sort" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="coll-thumb-url" class="w3-margin-left"><?php echo _('Avatar URL'); ?></label>
                    <input type="text" name="coll-thumb-url" id="coll-thumb-url" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="coll-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="coll-desc" id="summernote" class="w3-padding w3-margin-left"></textarea><br>
                    <label for="coll-rate" class="w3-margin-left"><?php echo _('Rating'); ?></label>
                    <input type="text" name="coll-rate" id="coll-rate" class="w3-input w3-padding w3-margin-left" maxlength="2"><br>
                    <label for="coll-art" class="w3-margin-left"><?php echo _('Artist'); ?></label>
                    <input type="text" name="coll-art" id="coll-art" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="coll-comp" class="w3-margin-left"><?php echo _('Company'); ?></label>
                    <input type="text" name="coll-comp" id="coll-comp" class="w3-input w3-padding w3-margin-left"><br>
                    <label for="coll-year" class="w3-margin-left"><?php echo _('Year'); ?></label>
                    <input type="year" name="coll-year" id="coll-year" class="w3-input w3-padding w3-margin-left"><br>
                    <input type="submit" name="coll-submit" id="coll-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('ADD COLLECTION'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
