<?php
/*
 * pub/dash/edit-category.php
 *
 * Allows users to edit an category.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

if (isset($_GET["catid"])) {
    $sel_id = $_GET["catid"];
} else {
    $sel_id = "";
}

if ($sel_id != '') {

    $getcategoryq = "SELECT * FROM ".TBLPREFIX."categories WHERE category_id='".$sel_id."'";
    $getcategoryquery = mysqli_query($dbconn,$getcategoryq);
    while ($getcategoryopt = mysqli_fetch_assoc($getcategoryquery)) {
        $categoryid             = $getcategoryopt['category_id'];
        $categoryname           = $getcategoryopt['category_name'];
        $categoryslug           = $getcategoryopt['category_slug'];
        $categorysort           = $getcategoryopt['category_sort_name'];
        $categoryavtr           = $getcategoryopt['category_avatar_url'];
        $categorydesc           = $getcategoryopt['category_description'];
    }

    // set the page title based on the title of the category file
    $pagetitle = _("Edit $categoryname « $website_name « ɧobgoblin");
}


/**
 * Form processing
 */
if (isset($_POST['cat-submit'])) {
    $id         = $_POST['cat-id'];
    $name       = nicetext($_POST['cat-name']);
    $namesort   = nicetext($_POST['cat-name-sort']);
    $thumburl   = trim(parse_url($_POST['cat-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['cat-desc']);

    if ($_POST['cat-slug'] !== '') {
        $newslug    = nicetext($_POST['cat-slug']);
    } else {
        $slug       = makeslug($_POST['cat-name']);


        $categoryslugqq = "SELECT * FROM ".TBLPREFIX."categories WHERE category_slug='".$slug."'";
        $categoryslugquery = mysqli_query($dbconn,$categoryslugqq);
        $categoryslugrows = mysqli_num_rows($categoryslugquery);
        $dupe = 2;

        if ($categoryslugrows === 1) {

            $categoryslugqq2 = "SELECT * FROM ".TBLPREFIX."categories WHERE category_slug LIKE '".$slug."-%'";
            $categoryslugquery2 = mysqli_query($dbconn,$categoryslugqq2);

            // Get the number of rows
            $categoryslugrows2 = mysqli_num_rows($categoryslugquery2);

            // add the number of rows to $dupe
            $newdupe = $dupe + $categoryslugrows2;

            $newslug = $slug."-".$newdupe;
        } else {
            $newslug = $slug;
        }
    }

    // update the category table
    $updcategoryq   = "UPDATE ".TBLPREFIX."categories SET category_name='".$name."', category_slug='".$newslug."', category_description='".$desc."', category_sort_name='".$namesort."', category_avatar_url='".$thumburl."' WHERE category_id='".$id."'";
    $updcategoryquery = mysqli_query($dbconn,$updcategoryq);


    // When we're done here, redirect to the category's page
    redirect($website_url."the-category.php?cat=".$newslug);
}


include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Edit category"); ?></h2>
                <form method="post" action="edit-category.php">
                    <input type="hidden" name="cat-id" id="cat-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="cat-slug" id="cat-slug" value="<?php echo $categoryslug; ?>">
                    <input type="text" name="cat-name" id="cat-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $categoryname; ?>" maxlength="255" required><br>
<?php
if ($categoryavtr !== '') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$categoryavtr."\" alt=\"".$categoryname."\" title=\"".$categoryname."\" class=\"w3-container w3-image\"><br><br>\n";
    echo "\t\t\t\t\t<label for=\"cat-thumb-url\" class=\"w3-margin-left\">"._('Change avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"cat-thumb-url\" id=\"cat-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$categoryavtr."\"><br>\n";
} else {
    echo "\t\t\t\t\t<label for=\"cat-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"cat-thumb-url\" id=\"cat-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$categoryavtr."\"><br>\n";
}
?>
                    <label for="cat-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="cat-name-sort" id="cat-name-sort" class="w3-input w3-padding w3-margin-left" value="<?php echo $categorysort; ?>"><br>
                    <label for="cat-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="cat-desc" id="summernote" class="w3-padding w3-margin-left"><?php echo $categorydesc; ?></textarea><br>
                    <input type="submit" name="cat-submit" id="cat-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE CATEGORY'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
