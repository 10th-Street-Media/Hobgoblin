<?php
/*
 * pub/dash/edit-tag.php
 *
 * Allows users to edit an tag.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

if (isset($_GET["tagid"])) {
    $sel_id = $_GET["tagid"];
} else {
    $sel_id = "";
}

if ($sel_id != '') {

    $gettagq = "SELECT * FROM ".TBLPREFIX."tags WHERE tag_id='".$sel_id."'";
    $gettagquery = mysqli_query($dbconn,$gettagq);
    while ($gettagopt = mysqli_fetch_assoc($gettagquery)) {
        $tagid      = $gettagopt['tag_id'];
        $tagname    = $gettagopt['tag_name'];
        $tagslug    = $gettagopt['tag_slug'];
        $tagsort    = $gettagopt['tag_sort_name'];
        $tagavtr    = $gettagopt['tag_avatar_url'];
        $tagdesc    = $gettagopt['tag_description'];
    }

    // set the page title based on the title of the tag file
    $pagetitle = _("Edit $tagname « $website_name « ɧobgoblin");
}


/**
 * Form processing
 */
if (isset($_POST['tag-submit'])) {
    $id         = $_POST['tag-id'];
    $name       = nicetext($_POST['tag-name']);
    $namesort   = nicetext($_POST['tag-name-sort']);
    $thumburl   = trim(parse_url($_POST['tag-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['tag-desc']);

    if ($_POST['tag-slug'] !== '') {
        $newslug    = nicetext($_POST['tag-slug']);
    } else {
        $slug       = makeslug($_POST['tag-name']);


        $tagslugqq = "SELECT * FROM ".TBLPREFIX."tags WHERE tag_slug='".$slug."'";
        $tagslugquery = mysqli_query($dbconn,$tagslugqq);
        $tagslugrows = mysqli_num_rows($tagslugquery);
        $dupe = 2;

        if ($tagslugrows === 1) {

            $tagslugqq2 = "SELECT * FROM ".TBLPREFIX."tags WHERE tag_slug LIKE '".$slug."-%'";
            $tagslugquery2 = mysqli_query($dbconn,$tagslugqq2);

            // Get the number of rows
            $tagslugrows2 = mysqli_num_rows($tagslugquery2);

            // add the number of rows to $dupe
            $newdupe = $dupe + $tagslugrows2;

            $newslug = $slug."-".$newdupe;
        } else {
            $newslug = $slug;
        }
    }


    // update the tag table
    $updtagq   = "UPDATE ".TBLPREFIX."tags SET tag_name='".$name."', tag_slug='".$newslug."', tag_description='".$desc."', tag_sort_name='".$namesort."', tag_avatar_url='".$thumburl."' WHERE tag_id='".$id."'";
    $updtagquery = mysqli_query($dbconn,$updtagq);


    // When we're done here, redirect to the tag's page
    redirect($website_url."the-tag.php?tag=".$newslug);
}


include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">
<?php echo $updtagq; ?>
                <h2 class="w3-padding"><?php echo _("Edit tag"); ?></h2>
                <form method="post" action="edit-tag.php">
                    <input type="hidden" name="tag-id" id="tag-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="tag-slug" id="tag-slug" value="<?php echo $tagslug; ?>">
                    <input type="text" name="tag-name" id="tag-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $tagname; ?>" maxlength="255" required><br>
<?php
if ($tagavtr !== '') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$tagavtr."\" alt=\"".$tagname."\" title=\"".$tagname."\" class=\"w3-container w3-image\"><br><br>\n";
    echo "\t\t\t\t\t<label for=\"tag-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"tag-thumb-url\" id=\"tag-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$tagavtr."\"><br>\n";
} else {
    echo "\t\t\t\t\t<label for=\"tag-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"tag-thumb-url\" id=\"tag-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$tagavtr."\"><br>\n";
}
?>
                    <label for="tag-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="tag-name-sort" id="tag-name-sort" class="w3-input w3-padding w3-margin-left" value="<?php echo $tagsort; ?>"><br>
                    <label for="tag-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="tag-desc" id="summernote" class="w3-padding w3-margin-left"><?php echo $tagdesc; ?></textarea><br>
                    <input type="submit" name="tag-submit" id="tag-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE tag'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
