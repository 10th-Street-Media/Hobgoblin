<?php
/*
 * pub/dash/artists.php
 *
 * A page listing all artists and creators of media on this instance.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

$pagetitle = _("Artists « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Artists"); ?></h2>

                <a href="add-artist.php" class="w3-button w3-button-hover w3-theme-d3 w3-margin-left"><?php echo _("Add an artist"); ?></a><br><br>

                <table class="w3-table-all w3-hoverable w3-margin-left">
                    <tr class="w3-theme-dark">
                        <th class="w3-center"><?php echo _('Artist'); ?></th>
                        <th class="w3-center"><?php echo _('Description'); ?></th>
                        <th class="w3-center"><?php echo _('Actions'); ?></th>
                    </tr>


<?php
/**
 * Check the database to see what artists we have
 */
$getartistlistq = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_name > '' ORDER BY artist_sort_name ASC";
$getartistlistquery = mysqli_query($dbconn,$getartistlistq);
while ($getartistlistopt = mysqli_fetch_assoc($getartistlistquery)) {
    $artistid       = $getartistlistopt['artist_id'];
    $artistname     = retext($getartistlistopt['artist_name']);
    $artistslug     = $getartistlistopt['artist_slug'];
    $artistsort     = retext($getartistlistopt['artist_sort_name']);
    $artistdesc     = retext($getartistlistopt['artist_description']);
    $artistavtr     = $getartistlistopt['artist_avatar_url'];

    echo "\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."artist/".$artistslug."\"><img src=\"".$website_url.$artistavtr."\" class=\"dash-avatar\"></a>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."artist/".$artistslug."\">".$artistsort."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t".$artistdesc."\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/edit-artist.php?aid=".$artistid."\">"._('Edit')."</a>\n";
    echo "\t\t\t\t\t\t\t&nbsp;|&nbsp;\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/delete-artist.php?aid=".$artistid."\">"._('Delete')."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t</tr>\n";
}
?>
                </table>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
