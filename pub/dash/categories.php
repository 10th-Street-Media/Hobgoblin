<?php
/*
 * pub/dash/categories.php
 *
 * A page listing all categories of media on this instance.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

$pagetitle = _("Categories « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Categories"); ?></h2>

                <a href="add-category.php" class="w3-button w3-button-hover w3-theme-d3 w3-margin-left"><?php echo _("Add a category"); ?></a><br><br>

                <table class="w3-table-all w3-hoverable w3-margin-left">
                    <tr class="w3-theme-dark">
                        <th class="w3-center"><?php echo _('Category'); ?></th>
                        <th class="w3-center"><?php echo _('Description'); ?></th>
                        <th class="w3-center"><?php echo _('Actions'); ?></th>
                    </tr>


<?php
/**
 * Check the database to see what categories we have
 */
$getcategorylistq = "SELECT * FROM ".TBLPREFIX."categories WHERE category_name > '' ORDER BY category_sort_name ASC";
$getcategorylistquery = mysqli_query($dbconn,$getcategorylistq);
while ($getcategorylistopt = mysqli_fetch_assoc($getcategorylistquery)) {
    $categoryid       = $getcategorylistopt['category_id'];
    $categoryname     = $getcategorylistopt['category_name'];
    $categoryslug     = $getcategorylistopt['category_slug'];
    $categorysort     = $getcategorylistopt['category_sort_name'];
    $categorydesc     = $getcategorylistopt['category_description'];
    $categoryavtr     = urldecode($getcategorylistopt['category_avatar_url']);

    echo "\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."category/".$categoryslug."\"><img src=\"".$website_url.$categoryavtr."\" class=\"dash-avatar\"></a>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."category/".$categoryslug."\">".$categoryname."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t".$categorydesc."\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t\t<td>\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/edit-category.php?catid=".$categoryid."\">"._('Edit')."</a>\n";
    echo "\t\t\t\t\t\t\t&nbsp;|&nbsp;\n";
    echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/delete-category.php?catid=".$categoryid."\">"._('Delete')."</a>\n";
    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t</tr>\n";
}
?>
                </table>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
