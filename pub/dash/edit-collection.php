<?php
/*
 * pub/dash/edit-collection.php
 *
 * Allows users to edit a collection.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

if (isset($_GET["cid"])) {
    $sel_id = $_GET["cid"];
} else {
    $sel_id = "";
}

if ($sel_id != '') {

    $getcollectionq = "SELECT * FROM ".TBLPREFIX."collections WHERE collection_id='".$sel_id."'";
    $getcollectionquery = mysqli_query($dbconn,$getcollectionq);
    while ($getcollectionopt = mysqli_fetch_assoc($getcollectionquery)) {
        $collectionid           = $getcollectionopt['collection_id'];
        $collectionname         = $getcollectionopt['collection_name'];
        $collectionslug         = $getsollectionopt['collection_slug'];
        $collectionsort         = $getcollectionopt['collection_sort_name'];
        $collectionavtr         = $getcollectionopt['collection_avatar_url'];
        $collectiondesc         = $getcollectionopt['collection_description'];
        $collectionrate         = $getcollectionopt['collection_rating'];
        $collectionarti         = $getcollectionopt['collection_artist'];
        $collectioncomp         = $getcollectionopt['collection_company'];
        $collectionyear         = $getcollectionopt['collection_year'];
    }

    // set the page title based on the title of the collection file
    $pagetitle = _("Edit $collectionname « $website_name « ɧobgoblin");
}


/**
 * Form processing
 */
if (isset($_POST['coll-submit'])) {
    $id         = $_POST['coll-id'];
    $name       = nicetext($_POST['coll-name']);
    $namesort   = nicetext($_POST['coll-name-sort']);
    $thumburl   = trim(parse_url($_POST['coll-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['coll-desc']);
    $rate       = nicetext($_POST['coll-rate']);
    $arti       = nicetext($_POST['coll-arti']);
    $comp       = nicetext($_POST['coll-comp']);
    $year       = $_POST['coll-year'];

    if ($_POST['coll-slug'] !== '') {
        $newslug    = nicetext($_POST['coll-slug']);
    } else {
        $slug       = makeslug($_POST['coll-name']);


        $collectionslugqq = "SELECT * FROM ".TBLPREFIX."collections WHERE collection_slug='".$slug."'";
        $collectionslugquery = mysqli_query($dbconn,$collectionslugqq);
        $collectionslugrows = mysqli_num_rows($collectionslugquery);
        $dupe = 2;

        if ($collectionslugrows === 1) {

            $collectionslugqq2 = "SELECT * FROM ".TBLPREFIX."collections WHERE collection_slug LIKE '".$slug."-%'";
            $collectionslugquery2 = mysqli_query($dbconn,$collectionslugqq2);

            // Get the number of rows
            $collectionslugrows2 = mysqli_num_rows($collectionslugquery2);

            // add the number of rows to $dupe
            $newdupe = $dupe + $collectionslugrows2;

            $newslug = $slug."-".$newdupe;
        } else {
            $newslug = $slug;
        }
    }



    // update the collection table
    $updcollectionq   = "UPDATE ".TBLPREFIX."collections SET collection_name='".$name."', collection_slug='".$newslug."', collection_description='".$desc."', collection_sort_name='".$namesort."', collection_avatar_url='".$thumburl."', collection_rating='".$rate."', collection_artist='".$arti."', collection_company='".$comp."', collection_year='".$year."' WHERE collection_id='".$id."'";
    $updcollectionquery = mysqli_query($dbconn,$updcollectionq);


    // When we're done here, redirect to the collection's page
    redirect($website_url."the-collection.php?title=".$newslug);
}


include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">
<?php echo $updcollectionq; ?>
                <h2 class="w3-padding"><?php echo _("Edit collection"); ?></h2>
                <form method="post" action="edit-collection.php">
                    <input type="hidden" name="coll-id" id="coll-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="coll-slug" id="coll-slug" value="<?php echo $collectionslug; ?>">
                    <input type="text" name="coll-name" id="coll-name" class="w3-input w3-padding w3-margin-left" value="<?php echo $collectionname; ?>" maxlength="255" required><br>
<?php
if ($collectionavtr !== '') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$collectionavtr."\" alt=\"".$collectionname."\" title=\"".$collectionname."\" class=\"w3-container w3-image\"><br><br>\n";
    echo "\t\t\t\t\t<label for=\"coll-thumb-url\" class=\"w3-margin-left\">"._('Change avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"coll-thumb-url\" id=\"coll-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$collectionavtr."\"><br>\n";
} else {
    echo "\t\t\t\t\t<label for=\"coll-thumb-url\" class=\"w3-margin-left\">"._('Avatar URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"coll-thumb-url\" id=\"coll-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$collectionavtr."\"><br>\n";
}
?>
                    <label for="coll-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="coll-name-sort" id="coll-name-sort" class="w3-input w3-padding w3-margin-left" value="<?php echo $collectionsort; ?>"><br>
                    <label for="coll-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="coll-desc" id="summernote" class="w3-padding w3-margin-left"><?php echo $collectiondesc; ?></textarea><br>
                    <label for="coll-arti" class="w3-margin-left"><?php echo _('Artist'); ?></label>
                    <input type="text" name="coll-arti" id="coll-arti" class="w3-input w3-padding w3-margin-left" value="<?php echo $collectionarti; ?>"><br>
                    <label for="coll-comp" class="w3-margin-left"><?php echo _('Company'); ?></label>
                    <input type="text" name="coll-comp" id="coll-comp" class="w3-input w3-padding w3-margin-left" value="<?php echo $collectioncomp; ?>"><br>
                    <label for="coll-year" class="w3-margin-left"><?php echo _('Year'); ?></label>
                    <input type="year" name="coll-year" id="coll-year" class="w3-input w3-padding w3-margin-left" value="<?php echo $collectionctry; ?>"><br>
                    <input type="submit" name="coll-submit" id="coll-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE COLLECTION'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
