<?php
/*
 * pub/dash/delete-collection.php
 *
 * A page where an collection can be deleted.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../conn.php";
include			"../../functions.php";
require			"../includes/database-connect.php";
require_once	"../includes/configuration-data.php";
require_once	"../includes/verify-cookies.php";

if (isset($_GET["cid"])) {
	$sel_id = $_GET["cid"];
} else {
	$sel_id = "";
}

/**
 * Form processing
 */
if (isset($_POST['collectiondelete'])) {

	$id		= $_POST['collection-id'];

	$collectionq	= "DELETE FROM ".TBLPREFIX."collections WHERE collection_id='".$id."'";
	$collectionquery = mysqli_query($dbconn,$collectionq);
	redirect($website_url."dash/collections.php");

} else if (isset($_POST['collectioncancel'])) {
	redirect($website_url."dash/collections.php");
}


$pagetitle = _("Delete collection « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete collection"); ?></h2>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete this collection?"); ?></b></p>
				<form method="post" action="delete-collection.php">
					<input type="hidden" name="collection-id" id="collection-id" value="<?php echo $sel_id; ?>">
					<table>
						<tr>
							<td><input type="submit" name="collectiondelete" id="collectiondelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="collectioncancel" id="collectioncancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
