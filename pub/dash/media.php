<?php
/*
 * pub/dash/media.php
 *
 * A page with all media on this instance.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

/**
 * Number of items per page
 */
 $perpg = $dashboard_index_items;

if(isset($_GET["pg"])){
$pg = intval($_GET["pg"]);
}
else {
$pg = 1;
}

$calc = $perpg * $pg;
$start = $calc - $perpg;


$pagetitle = _("Media library « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">

                <h2 class="w3-padding"><?php echo _("Media library"); ?></h2>

                <a href="add-media.php" class="w3-button w3-button-hover w3-theme-d3 w3-margin-left"><?php echo _("Add media"); ?></a><br><br>

                <table class="w3-table-all w3-hoverable w3-margin-left">
                    <tr class="w3-theme-dark">
                        <th class="w3-center"><?php echo _('Title'); ?></th>
                        <th class="w3-center"><?php echo _('Uploaded by'); ?></th>
                        <th class="w3-center"><?php echo _('Type'); ?></th>
                        <th class="w3-center"><?php echo _('Status'); ?></th>
                        <th class="w3-center"><?php echo _('Date'); ?></th>
                        <th class="w3-center"><?php echo _('Actions'); ?></th>
                    </tr>


<?php
/**
 * Check the database to see what media we have
 */
$getmedialistq = "SELECT * FROM ".TBLPREFIX."media ORDER BY media_date DESC LIMIT $start, $perpg";
$getmedialistquery = mysqli_query($dbconn,$getmedialistq);
$getmedialistrows = mysqli_num_rows($getmedialistquery);
if ($getmedialistrows) {

    $i = 0;

    while ($getmedialistopt = mysqli_fetch_assoc($getmedialistquery)) {
        $mediaid    = $getmedialistopt['media_id'];
        $mediauser  = retext($getmedialistopt['user_name']);
        $mediaurl   = $getmedialistopt['media_url'];
        $mediadate  = $getmedialistopt['media_date'];
        $mediatitle = retext($getmedialistopt['media_title']);
        $mediaslug  = $getmedialistopt['media_slug'];
        $mediatype  = $getmedialistopt['media_type'];
        $mediathumb = urldecode($getmedialistopt['media_thumbnail_url']);
        $mediastat  = $getmedialistopt['media_status'];

        echo "\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t<td>\n";
        if ($mediatype == 'IMAGE') {
            echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url.$mediaurl."\" class=\"dash-avatar\"></a>\n";
        } else if ($mediatype == 'AUDIO') {
            if ($mediathumb == '') {
                echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url."images/generic-audio-600.png\" class=\"dash-avatar\"></a>\n";
            } else {
                echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url.$mediathumb."\" class=\"dash-avatar\"></a>\n";
            }
        } else if ($mediatype == 'VIDEO') {
            if ($mediathumb == '') {
                echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url."images/generic-video-600.png\" class=\"dash-avatar\"></a>\n";
            } else {
                echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url.$mediathumb."\" class=\"dash-avatar\"></a>\n";
            }
        } else if ($mediatype == 'DOCUMENT') {
            echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url."images/generic-document-600.png\" class=\"dash-avatar\"></a>\n";
        } else if ($mediatype == 'PACKAGE') {
            echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\"><img src=\"".$website_url."images/generic-package-600.png\" class=\"dash-avatar\"></a>\n";
        }
        echo "\t\t\t\t\t\t\t<a href=\"".$website_url."media/".$mediaslug."\">".$mediatitle."</a>\n";
        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t\t<td>\n";
        echo "\t\t\t\t\t\t\t<a href=\"".$website_url."users/".$mediauser."\">".$mediauser."</a>\n";
        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t\t<td>\n";
        echo "\t\t\t\t\t\t\t".$mediatype."\n";
        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t\t<td>\n";
        echo "\t\t\t\t\t\t\t".$mediastat."\n";
        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t\t<td>\n";
        echo "\t\t\t\t\t\t\t".$mediadate."\n";
        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t\t<td>\n";
        echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/edit-media.php?mdid=".$mediaid."\">"._('Edit')."</a>\n";
        echo "\t\t\t\t\t\t\t&nbsp;|&nbsp;\n";
        echo "\t\t\t\t\t\t\t<a href=\"".$website_url."dash/delete-media.php?mdid=".$mediaid."\">"._('Delete')."</a>\n";
        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t</tr>\n";
    }
}
?>
                </table>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
 //   if ($i > 0) {
        echo "\t\t\t<table cellspacing=\"2\" cellpadding=\"2\" align=\"center\">\n";
        echo "\t\t\t\t<tbody>\n";
        echo "\t\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t\t<td align=\"center\">\n";

        if (isset($pg)) {
            $pagecountquery = mysqli_query($dbconn,"SELECT COUNT(*) AS Total FROM ".TBLPREFIX."media");
            $pagecount = mysqli_num_rows($pagecountquery);

            if ($pagecount) {
                $rs = mysqli_fetch_assoc($pagecountquery);
                $total = $rs["Total"];
            }

            $totalPages = ceil($total / $perpg);

            if($pg <=1 ){
                echo "\t\t\t\t\t\t\t<span class=\"w3-padding\" style='font-weight: bold;'>Prev</span>\n";
            } else {
                $j = $pg - 1;
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$j'>Prev</a></span>\n";
            }

            for($i=1; $i <= $totalPages; $i++) {
                if($i<>$pg) {
                    echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$i'>$i</a></span>\n";
                } else {
                    echo "\t\t\t\t\t\t\t<span class=\"w3-button w3-green\" style='font-weight: bold;'>$i</span>\n";
                }
            } // end for $i=1

            if($pg == $totalPages ) {
                echo "\t\t\t\t\t\t\t<span class=\"w3-padding\" style='font-weight: bold;'>Next</span>\n";
            } else {
                $j = $pg + 1;
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$j'>Next</a></span>\n";
            }
        }

        echo "\t\t\t\t\t\t</td>\n";
        echo "\t\t\t\t\t</tr>\n";
        echo "\t\t\t\t</tbody>\n";
        echo "\t\t\t</table>\n";
    //}
?>


<?php
include "footer.php";
?>
