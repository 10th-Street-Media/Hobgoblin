<?php
/*
 * pub/dash/delete-tag.php
 *
 * A page where an tag can be deleted.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../conn.php";
include			"../../functions.php";
require			"../includes/database-connect.php";
require_once	"../includes/configuration-data.php";
require_once	"../includes/verify-cookies.php";

if (isset($_GET["tagid"])) {
	$sel_id = $_GET["tagid"];
} else {
	$sel_id = "";
}

/**
 * Form processing
 */
if (isset($_POST['tagdelete'])) {

	$id		= $_POST['tag-id'];

	$tagq	= "DELETE FROM ".TBLPREFIX."tags WHERE tag_id='".$id."'";
	$tagquery = mysqli_query($dbconn,$tagq);
	redirect($website_url."dash/tags.php");

} else if (isset($_POST['tagcancel'])) {
	redirect($website_url."dash/tags.php");
}


$pagetitle = _("Delete tag « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete tag"); ?></h2>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete this tag?"); ?></b></p>
				<form method="post" action="delete-tag.php">
					<input type="hidden" name="tag-id" id="tag-id" value="<?php echo $sel_id; ?>">
					<table>
						<tr>
							<td><input type="submit" name="tagdelete" id="tagdelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="tagcancel" id="tagcancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
