<?php
/*
 * pub/dash/add-company.php
 *
 * Allows users to add a company.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";



/**
 * Form processing
 */
if (isset($_POST['comp-submit'])) {
    $name       = nicetext($_POST['comp-name']);
    $slug       = makeslug($_POST['comp-name']);
    $namesort   = nicetext($_POST['comp-name-sort']);
    $thumburl   = trim(parse_url($_POST['comp-thumb-url'], PHP_URL_PATH), '/');
    $desc       = nicetext($_POST['comp-desc']);
    $rate       = nicetext($_POST['comp-rate']);
    $start      = $_POST['comp-start'];
    $end        = $_POST['comp-end'];
    $ctry       = nicetext($_POST['comp-ctry']);
    $indu       = nicetext($_POST['comp-indu']);
    $asso       = nicetext($_POST['comp-asso']);


    // add to the companies table
    $addcompanyq   = "INSERT INTO ".TBLPREFIX."companies (company_id, company_name, company_slug, company_sort_name, company_avatar_url, company_description, company_rating, company_start_year, company_end_year, company_country, company_industries, company_associates) VALUES ('', '".$name."', '".$slug."', '".$namesort."', '".$thumburl."', '".$desc."', '".$rate."', '".$start."', '".$end."', '".$ctry."', '".$indu."', '".$asso."')";
    $addcompanyquery = mysqli_query($dbconn,$addcompanyq);


    // When we're done here, redirect to the company's page
    redirect($website_url."the-company.php?name=".$slug);
}

$pagetitle = _("Add a company « $website_name « ɧobgoblin");

include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">
                <h2 class="w3-padding"><?php echo _("Add a company"); ?></h2>
                <form method="post" action="add-company.php">
                    <label for="comp-name" class="w3-margin-left"><?php echo _('Company name'); ?></label>
                    <input type="text" name="comp-name" id="comp-name" class="w3-input w3-padding w3-margin-left" maxlength="255" required tabindex="1"><br>
                    <label for="comp-name-sort" class="w3-margin-left"><?php echo _('Sort name'); ?></label>
                    <input type="text" name="comp-name-sort" id="comp-name-sort" class="w3-input w3-padding w3-margin-left" tabindex="2"><br>
                    <label for="comp-thumb-url" class="w3-margin-left"><?php echo _('Avatar URL'); ?></label>
                    <input type="text" name="comp-thumb-url" id="comp-thumb-url" class="w3-input w3-padding w3-margin-left" tabindex="3"><br>
                    <label for="comp-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="comp-desc" id="summernote" class="w3-padding w3-margin-left" tabindex="4"></textarea><br>
                    <label for="comp-start" class="w3-margin-left"><?php echo _('Start year'); ?></label>
                    <input type="year" name="comp-start" id="comp-start" class="w3-input w3-padding w3-margin-left" tabindex="5"><br>
                    <label for="comp-end" class="w3-margin-left"><?php echo _('End year'); ?></label>
                    <input type="year" name="comp-end" id="comp-end" class="w3-input w3-padding w3-margin-left" tabindex="6"><br>
                    <label for="comp-ctry" class="w3-margin-left"><?php echo _('Country'); ?></label>
                    <input type="text" name="comp-ctry" id="comp-ctry" class="w3-input w3-padding w3-margin-left" tabindex="7"><br>
                    <label for="comp-indu" class="w3-margin-left"><?php echo _('Industries'); ?></label>
                    <input type="text" name="comp-indu" id="comp-indu" class="w3-input w3-padding w3-margin-left" tabindex="8"><br>
                    <label for="comp-asso" class="w3-margin-left"><?php echo _('Associates'); ?></label>
                    <input type="text" name="comp-asso" id="comp-asso" class="w3-input w3-padding w3-margin-left" tabindex="9"><br>
                    <input type="submit" name="comp-submit" id="comp-submit" class="w3-theme-dark w3-button w3-margin-left" tabindex="10" value="<?php echo _('ADD COMPANY'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
