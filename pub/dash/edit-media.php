<?php
/*
 * pub/dash/edit-media.php
 *
 * Allows users to edit a page.
 *
 * since Hobgoblin version 0.1
 */

include_once    "../../conn.php";
include         "../../functions.php";
require         "../includes/database-connect.php";
require_once    "../includes/configuration-data.php";
require_once    "../includes/verify-cookies.php";

if (isset($_GET["mdid"])) {
    $sel_id = $_GET["mdid"];
} else {
    $sel_id = "";
}

if ($sel_id != '') {

    $getmediaq = "SELECT * FROM ".TBLPREFIX."media WHERE media_id='".$sel_id."'";
    $getmediaquery = mysqli_query($dbconn,$getmediaq);
    while ($getmediaopt = mysqli_fetch_assoc($getmediaquery)) {
        $mediaid            = $getmediaopt['media_id'];
        $mediauser          = $getmediaopt['user_name'];
        $mediadate          = $getmediaopt['media_date'];
        $mediaurl           = $getmediaopt['media_url'];
        $mediatitle         = retext($getmediaopt['media_title']);
        $mediaslug          = $getmediaopt['media_slug'];
        $mediaalt           = retext($getmediaopt['media_alt']);
        $mediacapt          = retext($getmediaopt['media_caption']);
        $mediadesc          = retext($getmediaopt['media_description']);
        $mediastat          = $getmediaopt['media_status'];
        $mediatype          = $getmediaopt['media_type'];
        $mediamod           = $getmediaopt['media_modified_date'];
        $mediatags          = retext($getmediaopt['media_tags']);
        $mediacats          = retext($getmediaopt['media_categories']);
        $mediaid3           = $getmediaopt['media_id3_genre'];
        $mediaarti          = retext($getmediaopt['media_artist']);
        $mediacomp          = retext($getmediaopt['media_company']);
        $mediacoll          = retext($getmediaopt['media_collection_id']);
        $mediayear          = $getmediaopt['media_year'];
        $mediarate          = $getmediaopt['media_rating'];
        $mediabyte          = $getmediaopt['media_bytes'];
        $mediawide          = $getmediaopt['media_image_width'];
        $mediatall          = $getmediaopt['media_image_height'];
        $mediathumb         = $getmediaopt['media_thumbnail_url'];
        $medialayo          = $getmediaopt['media_layout'];
        $mediarunt          = $getmediaopt['media_runtime'];
        $mediatitlesort     = retext($getmediaopt['media_title_sort_order']);
        $mediacomm          = $getmediaopt['media_comments_open'];
        $mediawarn          = $getmediaopt['media_content_warning'];
    }

    // set the page tite based on the title of the media file
    $pagetitle = _("Edit $mediatitle « $website_name « ɧobgoblin");
}


/**
 * Form processing
 */
if (isset($_POST['med-submit'])) {
    $id         = $_POST['med-id'];
    $title      = nicetext($_POST['med-title']);
    $titlesort  = nicetext($_POST['med-title-sort']);
    $thumburl   = trim(parse_url($_POST['med-thumb-url'], PHP_URL_PATH), '/');
    $thumbwarn  = $_POST['med-thumb-warn'];
    $alt        = nicetext($_POST['med-alt']);
    $capt       = nicetext($_POST['med-capt']);
    $desc       = nicetext($_POST['med-desc']);
    $arti       = makecoll($_POST['med-arti']);
    $comp       = makecoll($_POST['med-comp']);
    $coll       = makecoll($_POST['med-coll']);
    $tags       = makecoll($_POST['med-tags']);
    $cats       = makecoll($_POST['med-cats']);
    $year       = nicetext($_POST['med-year']);
    $rate       = nicetext($_POST['med-rate']);
    $stat       = $_POST['med-stat'];
    $warn       = $_POST['med-warn'];
    $now        = date("Y-m-d H:i:s");

    if ($_POST['med-slug'] !== '') {
        $newslug    = nicetext($_POST['med-slug']);
    } else {
        $slug       = makeslug($_POST['med-title']);


        // Let's double check the slug and see if it's unique
        $mediaslugqq = "SELECT * FROM ".TBLPREFIX."media WHERE media_slug='".$slug."'";
        $mediaslugquery = mysqli_query($dbconn,$mediaslugqq);
        $mediaslugrows = mysqli_num_rows($mediaslugquery);
        $dupe = 2;

        if ($mediaslugrows === 1) {

            $mediaslugqq2 = "SELECT * FROM ".TBLPREFIX."media WHERE media_slug LIKE '".$slug."-%'";
            $mediaslugquery2 = mysqli_query($dbconn,$mediaslugqq2);

            // Get the number of rows
            $mediaslugrows2 = mysqli_num_rows($mediaslugquery2);

            // add the number of rows to $dupe
            $newdupe = $dupe + $mediaslugrows2;

            $newslug = $slug."-".$newdupe;

        } else {
            $newslug = $slug;
        }
    }

    // update the media table
    $updmediaq   = "UPDATE ".TBLPREFIX."media SET media_title='".$title."', media_slug='".$newslug."', media_alt='".$alt."', media_caption='".$capt."', media_description='".$desc."', media_title_sort_order='".$titlesort."', media_artist='".$arti."', media_company='".$comp."', media_modified_date='".$now."', media_tags='".$tags."', media_categories='".$cats."', media_year='".$year."', media_rating='".$rate."', media_collection_id='".$coll."', media_status='".$stat."', media_thumbnail_url='".$thumburl."', media_thumbnail_warning='".$thumbwarn."', media_content_warning='".$warn."' WHERE media_id='".$id."'";
    $updmediaquery = mysqli_query($dbconn,$updmediaq);


    /**
     * artist table
     *
     * split $arti into an array
     * for each artist, see if it is in the artist table
     * if they are in the table, do nothing
     * if they are not in the table, add them
     */
    if ($arti !== '') {
        // split $arti
        $artists = preg_split('/,/i', $arti);

        // see if these artists are in the artist table
        foreach ($artists as $artist) {
            $artistq = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_name='".$artist."'";
            $artistquery = mysqli_query($dbconn,$artistq);
            if (mysqli_num_rows($artistquery) == 0) {
                $artistslug = makeslug($artist);
                $addnewartistq = "INSERT INTO ".TBLPREFIX."artists (artist_name, artist_slug) VALUES ('".$artist."', '".$artistslug."')";
                $addnewartistquery = mysqli_query($dbconn,$addnewartistq);
            }
        }
    }

    /**
     * category table
     *
     * split $cats into an array
     * for each category, see if it is in the category table
     * if they are in the table, do nothing
     * if they are not in the table, add them
     */
    if ($cats !== '') {
        // split $cats
        $categories = preg_split('/,/i', $cats);

        // see if these categories are in the category table
        foreach ($categories as $category) {
            $categoryq = "SELECT * FROM ".TBLPREFIX."categories WHERE category_name='".$category."'";
            $categoryquery = mysqli_query($dbconn,$categoryq);
            if (mysqli_num_rows($categoryquery) == 0) {
                $categoryslug = makeslug($category);
                $addnewcategoryq = "INSERT INTO ".TBLPREFIX."categories (category_name, category_slug) VALUES ('".$category."', '".$categoryslug."')";
                $addnewcategoryquery = mysqli_query($dbconn,$addnewcategoryq);
            }
        }
    }

    /**
     * company table
     *
     * split $comp into an array
     * for each company, see if it is in the company table
     * if they are in the table, do nothing
     * if they are not in the table, add them
     */
    if ($comp !== '') {
        // split $comp
        $companies = preg_split('/,/i', $comp);

        // see if these companies are in the company table
        foreach ($companies as $company) {
            $companyq = "SELECT * FROM ".TBLPREFIX."companies WHERE company_name='".$company."'";
            $companyquery = mysqli_query($dbconn,$companyq);
            if (mysqli_num_rows($companyquery) == 0) {
                $companyslug = makeslug($company);
                $addnewcompanyq = "INSERT INTO ".TBLPREFIX."companies (company_name, company_slug) VALUES ('".$company."', '".$companyslug."')";
                $addnewcompanyquery = mysqli_query($dbconn,$addnewcompanyq);
            }
        }
    }

    /**
     * collection table
     *
     * split $coll into an array
     * for each collection, see if it is in the collection table
     * if they are in the table, do nothing
     * if they are not in the table, add them
     */
    if ($coll !== '') {
        // split $coll
        $collections = preg_split('/,/i', $coll);

        // see if these collections are in the collection table
        foreach ($collections as $collection) {
            $collectionq = "SELECT * FROM ".TBLPREFIX."collections WHERE collection_name='".$collection."'";
            $collectionquery = mysqli_query($dbconn,$collectionq);
            if (mysqli_num_rows($collectionquery) == 0) {
                $collectionslug = makeslug($collection);
                $addnewcollectionq = "INSERT INTO ".TBLPREFIX."collections (collection_name, collection_slug) VALUES ('".$collection."', '".$collectionslug."')";
                $addnewcollectionquery = mysqli_query($dbconn,$addnewcollectionq);
            }
        }
    }

    /**
     * tag table
     *
     * split $tags into an array
     * for each tag, see if it is in the tag table
     * if they are in the table, do nothing
     * if they are not in the table, add them
     */
    if ($tags !== '') {
        // split $tags
        $tags = preg_split('/,/i', $tags);

        // see if these tags are in the tag table
        foreach ($tags as $tag) {
            $tagq = "SELECT * FROM ".TBLPREFIX."tags WHERE tag_name='".$tag."'";
            $tagquery = mysqli_query($dbconn,$tagq);
            if (mysqli_num_rows($tagquery) == 0) {
                $tagslug = makeslug($tag);
                $addnewtagq = "INSERT INTO ".TBLPREFIX."tags (tag_name, tag_slug) VALUES ('".$tag."', '".$tagslug."')";
                $addnewtagquery = mysqli_query($dbconn,$addnewtagq);
            }
        }
    }

    // When we're done here, redirect to the media's page
    redirect($website_url."the-media.php?title=".$newslug);
#echo "<br><br>\n\n".$updmediaq."<br>\n";
}


#$pagetitle = _("Edit media « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

            <article class="w3-padding w3-col s12 m8 l10">
<?php echo $updpostq; ?>
                <h2 class="w3-padding"><?php echo _("Edit media"); ?></h2>
                <form method="post" action="edit-media.php">
                    <input type="hidden" name="med-id" id="med-id" value="<?php echo $sel_id; ?>">
                    <input type="hidden" name="med-slug" id="med-slug" value="<?php echo $mediaslug; ?>">
                    <input type="text" name="med-title" id="med-title" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediatitle; ?>" maxlength="255" required><br>
<?php
/**
 * If this is an image, display it.
 * If this is a different media type, display the appropriate player or a generic icon.
 */
if ($mediatype == 'IMAGE') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$mediaurl."\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"><br><br>\n";
} else if ($mediatype == 'VIDEO') {
    echo "\t\t\t\t\t<video controls class=\"w3-container w3-image\">\n";
    echo "\t\t\t\t\t\t<source src=\"".$website_url.$mediaurl."\">\n";
    echo "\t\t\t\t\t</video><br><br>\n";
} else if ($mediatype == 'AUDIO') {
    if ($mediathumb == '') {
        echo "\t\t\t\t\t<object data=\"images/generic-audio-600.png\" width=\"600\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"></object><br><br>\n";
    } else {
        echo "\t\t\t\t\t<object data=\"".$website_url.$mediathumb."\" width=\"600\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"></object><br><br>\n";
    }
    echo "\t\t\t\t\t<audio style=\"width:600px\" controls class=\"w3-container w3-image\">\n";
    echo "\t\t\t\t\t\t<source src=\"".$website_url.$mediaurl."\">\n";
    echo "\t\t\t\t\t</audio><br><br>\n";
}
?>
<?php
/**
 * If it's audio or video, let's link to a thumbnail, poster, album cover, or something
 */
if ($mediatype == 'VIDEO' || $mediatype == 'AUDIO') {
    echo "\t\t\t\t\t<label for=\"med-thumb-url\" class=\"w3-margin-left\">"._('Artwork URL')."</label>\n";
    echo "\t\t\t\t\t<input type=\"text\" name=\"med-thumb-url\" id=\"med-thumb-url\" class=\"w3-input w3-padding w3-margin-left\" value=\"".$website_url.$mediathumb."\"><br>\n";
    echo "\t\t\t\t\t<label for=\"med-thumb-warn\" class=\"w3-margin-left\">"._('Artwork content warning?')."</label>\n";
    echo "<input type=\"radio\" name=\"med-thumb-warn\" id=\"med-thumb-warn\" class=\"w3-radio w3-padding w3-margin-left\"";
    if ($mediawarn == "1") {
        echo "checked";
    }
    echo "value=\"1\">"._("YES")."\n";
    echo "<input type=\"radio\" name=\"med-thumb-warn\" id=\"med-thumb-warn\" class=\"w3-radio w3-padding w3-margin-left\"";
    if ($mediawarn == "0") {
        echo "checked";
    }
    echo "value=\"0\">"._("NO")."\n";
    echo "<br><br>";
}
?>
                    <label for="med-alt" class="w3-margin-left"><?php echo _('Alt text'); ?></label>
                    <input type="text" name="med-alt" id="med-alt" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediaalt; ?>"><br>
                    <label for="med-capt" class="w3-margin-left"><?php echo _('Caption'); ?></label>
                    <input type="text" name="med-capt" id="med-capt" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediacapt; ?>"><br>
                    <label for="med-desc" class="w3-margin-left"><?php echo _('Description'); ?></label>
                    <textarea name="med-desc" id="summernote" class="w3-padding w3-margin-left"><?php echo $mediadesc; ?></textarea><br>
                    <label for="med-title-sort" class="w3-margin-left"><?php echo _('Title sort order'); ?></label>
                    <input type="text" name="med-title-sort" id="med-title-sort" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediatitlesort; ?>" maxlength="255"><br>
                    <label for="med-arti" class="w3-margin-left"><?php echo _('Artists'); ?></label>
                    <input type="text" name="med-arti" id="med-arti" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediaarti; ?>"><br>
                    <label for="med-comp" class="w3-margin-left"><?php echo _('Companies'); ?></label>
                    <input type="text" name="med-comp" id="med-comp" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediacomp; ?>"><br>
                    <label for="med-coll" class="w3-margin-left"><?php echo _('Collections'); ?></label>
                    <input type="text" name="med-coll" id="med-coll" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediacoll; ?>"><br>
                    <label for="med-year" class="w3-margin-left"><?php echo _('Year'); ?></label>
                    <input type="text" name="med-year" id="med-year" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediayear; ?>" maxlength="5"><br>
                    <label for="med-rate" class="w3-margin-left"><?php echo _('Rating'); ?></label>
                    <input type="text" name="med-rate" id="med-rate" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediarate; ?>" maxlength="2"><br>
                    <label for="med-tags" class="w3-margin-left"><?php echo _('Tags'); ?></label>
                    <input type="text" name="med-tags" id="med-tags" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediatags; ?>"><br>
                    <label for="med-cats" class="w3-margin-left"><?php echo _('Categories'); ?></label>
                    <input type="text" name="med-cats" id="med-cats" class="w3-input w3-padding w3-margin-left" value="<?php echo $mediacats; ?>"><br>
                    <label for="med-stat" class="w3-margin-left"><?php echo _('Status'); ?></label>
                    <select name="med-stat" id="med-stat" class="w3-input w3-padding w3-margin-left">
<?php
// get the list of post statuses from the DB
$statusq = "SELECT * FROM ".TBLPREFIX."post_statuses ORDER BY post_status_name ASC";
$statusquery = mysqli_query($dbconn,$statusq);
while ($statusopt = mysqli_fetch_assoc($statusquery)) {
    if ($mediastat === $statusopt['post_status_name']) {
        echo "\t\t\t\t\t\t<option value=\"".$statusopt['post_status_name']."\" selected>".$statusopt['post_status_name']."</option>\n";
    } else {
        echo "\t\t\t\t\t\t<option value=\"".$statusopt['post_status_name']."\">".$statusopt['post_status_name']."</option>\n";
    }
}
?>
                    </select><br>
                    <label for="med-warn" class="w3-margin-left"><?php echo _('Content warning?'); ?></label>
                    <input type="radio" name="med-warn" id="med-warn" class="w3-radio w3-padding w3-margin-left" <?php if ($mediawarn == "1") {echo "checked"; } ?> value="1"><?php echo _("YES")."\n"; ?>
                    <input type="radio" name="med-warn" id="med-warn" class="w3-radio w3-padding w3-margin-left" <?php if ($mediawarn == "0") {echo "checked"; } ?> value="0"><?php echo _("NO")."\n"; ?>
                    <br><br>
                    <input type="submit" name="med-submit" id="med-submit" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('UPDATE MEDIA'); ?>">
                </form>
            </article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic','underline','strikethrough','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul','ol','paragraph']],
            ['table', ['table']],
            ['insert', ['hr','link','picture','video']],
            ['view', ['fullscreen','codeview','help']]
        ],
        height: 240
    });
});
</script>

<?php
include "footer.php";
?>
