<?php
/*
 * pub/dash/delete-artist.php
 *
 * A page where an artist can be deleted.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../conn.php";
include			"../../functions.php";
require			"../includes/database-connect.php";
require_once	"../includes/configuration-data.php";
require_once	"../includes/verify-cookies.php";

if (isset($_GET["aid"])) {
	$sel_id = $_GET["aid"];
} else {
	$sel_id = "";
}

/**
 * Form processing
 */
if (isset($_POST['artistdelete'])) {

	$id		= $_POST['artist-id'];

	$artistq	= "DELETE FROM ".TBLPREFIX."artists WHERE artist_id='".$id."'";
	$artistquery = mysqli_query($dbconn,$artistq);
	redirect($website_url."dash/artists.php");

} else if (isset($_POST['artistcancel'])) {
	redirect($website_url."dash/artists.php");
}


$pagetitle = _("Delete artist « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete artist"); ?></h2>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete this artist?"); ?></b></p>
				<form method="post" action="delete-artist.php">
					<input type="hidden" name="artist-id" id="artist-id" value="<?php echo $sel_id; ?>">
					<table>
						<tr>
							<td><input type="submit" name="artistdelete" id="artistdelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="artistcancel" id="artistcancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
