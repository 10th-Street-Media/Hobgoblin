<?php
/*
 * pub/dash/delete-media.php
 *
 * A page where a media file can be deleted.
 *
 * since Hobgoblin version 0.1
 */

include_once	"../../conn.php";
include			"../../functions.php";
require			"../includes/database-connect.php";
require_once	"../includes/configuration-data.php";
require_once	"../includes/verify-cookies.php";

if (isset($_GET["mdid"])) {
	$sel_id = $_GET["mdid"];
} else {
	$sel_id = "";
}

/**
 * Form processing
 */
if (isset($_POST['mediadelete'])) {

	$id		= $_POST['media-id'];

	$mediaq	= "DELETE FROM ".TBLPREFIX."media WHERE media_id='".$id."'";
	$mediaquery = mysqli_query($dbconn,$mediaq);
	redirect($website_url."dash/media.php");

} else if (isset($_POST['mediacancel'])) {
	redirect($website_url."dash/media.php");
}


$pagetitle = _("Delete media « $website_name « ɧobgoblin");
include "header.php";
include "nav.php";
?>

			<article class="w3-padding w3-col s12 m8 l10">

				<h2 class="w3-padding"><?php echo _("Delete media"); ?></h2>
				<p class="w3-padding"><?php echo _('Deleting a media file will remove it from this instance only. If it has been shared or reposted by users on this or other instances, those versions will still exist.'); ?></p>
				<p class="w3-padding"><b><?php echo _("Are you sure you want to delete this media file?"); ?></b></p>
				<form method="post" action="delete-media.php">
					<input type="hidden" name="media-id" id="media-id" value="<?php echo $sel_id; ?>">
					<table>
						<tr>
							<td><input type="submit" name="mediadelete" id="mediadelete" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('YES'); ?>"></td>
							<td><input type="submit" name="mediacancel" id="mediacancel" class="w3-theme-dark w3-button w3-margin-left" value="<?php echo _('NO'); ?>"></td>
						</tr>
					</table>
				</form>
			</article> <!-- end article (It's not really an article, but it serves the same purpose.) -->

<?php
include "footer.php";
?>
