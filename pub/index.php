<?php
/**
 * pub/index.php
 *
 * This is the main page for Hobgoblin and serves several purposes.
 * If Hobgoblin is not installed, it triggers installation.
 * If Hobgoblin is installed, it calls index.php from the active theme.
 * since Hobgoblin version 0.1
 *
 */

/**
 * If ../conn.php does not exist...
 */
if(!file_exists("../conn.php")) {

    /**
     * ...and conn.php does not exist...
     */
    if (!file_exists("conn.php")) {

        /**
         * redirect user to install page.
         */
        header("Location: dash/admin/install.php");
    } else {

        /**
         * conn.php does exist
         * redirect user to post-install page
         */
        header("Location: dash/admin/post-install.php");
    }
} else {

    /**
     * ../conn.php does exist
     * Let us include it, then verify its constants
     */

    include "../conn.php";

    /**
     * if $global_count === 5 at the end then all global variables are set.
     * if $global_count < 5 then something is missing.
     */

    $global_count = 0;

    if (DBHOST != "") {
        #echo DBHOST;
        $global_count++;
    }

    if (DBNAME != "") {
        $global_count++;
    }

    if (DBUSER != "") {
        $global_count++;
    }

    if (DBPASS != "") {
        $global_count++;
    }

    if (SITEKEY != "") {
        $global_count++;
    }
}

include         "../functions.php";
require         "includes/database-connect.php";
require_once    "includes/configuration-data.php";
include_once    "nodeinfo/version.php";

// see if a session is set. If so, redirect them to their dashboard.

if (isset($_COOKIE['uname'])) {
    if ($_COOKIE['uname'] != '') {
        $visitortitle = $_COOKIE['uname'];
    }
} else {
    $visitortitle = _('Guest');
}


$pagetitle = $website_name;
$objdescription = $website_description;

include_once "includes/fed-header.php";
include_once 'includes/fed-nav.php';
?>
<?php
include "feeds.php";
include "nodeinfo.php";
?>
            <div class="w3-col w3-panel w3-cell m10">
<?php

/**
 * Number of items per page
 */
$perpg = $frontend_items;

/**
 * Get the page number, or set it to 1 if it isn't defined.
 */
if(isset($_GET["pg"])){
$pg = intval($_GET["pg"]);
}
else {
$pg = 1;
}

/**
 * Calculate the starting point for the query LIMIT
 */
$calc = $perpg * $pg;
$start = $calc - $perpg;



/**
 * Get the media items for this page.
 */
 $getmediaitemsq = "SELECT * FROM ".TBLPREFIX."media WHERE media_status='PUBLIC' ORDER BY media_date DESC LIMIT $start, $perpg";
 $getmediaitemsquery = mysqli_query($dbconn,$getmediaitemsq);
 $getmediaitemsrows = mysqli_num_rows($getmediaitemsquery);
 if ($getmediaitemsrows) {

     $i = 0;

     while ($getmediaitemsopt = mysqli_fetch_assoc($getmediaitemsquery)) {

         $mediaid       = $getmediaitemsopt['media_id'];
         $mediatitle    = $getmediaitemsopt['media_title'];
         $mediadate     = $getmediaitemsopt['media_date'];
         $mediauser     = $getmediaitemsopt['user_name'];
         $mediaurl      = $getmediaitemsopt['media_url'];
         $mediaslug     = $getmediaitemsopt['media_slug'];
         $mediaalt      = $getmediaitemsopt['media_alt'];
         $mediathumb    = urldecode($getmediaitemsopt['media_thumbnail_url']);
         $mediathumbcw  = $getmediaitemsopt['media_thumbnail_warning'];
         $mediatype     = $getmediaitemsopt['media_type'];
         $mediawarn     = $getmediaitemsopt['media_content_warning'];

        echo "\t\t\t<article class=\"w3-panel\">\n";
        echo "\t\t\t\t<h2 class=\"w3-text-theme w3-container w3-bar";
        echo "\"><a href=\"".$website_url."media/".$mediaslug."\">".$mediatitle."</a></h2>\n";
        echo "\t\t\t\t<span class=\"w3-container w3-block\">"._('Posted on ').$mediadate._(' by ')."<a href=\"".$website_url."users/".$mediauser."\">".$mediauser."</a></span><br>\n";
        echo "\t\t\t\t<div class=\"w3-container w3-block\">\n";
        /**
         * Change the view depending on the media type
         */
        if ($mediatype == 'IMAGE') {
            echo "\t\t\t\t\t<img src=\"".$website_url.$mediaurl."\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"";
            if ($mediawarn == "1") {
                echo " style=\"filter:blur(25px)\"";
            }
            echo "><br><br>\n";
        } else if ($mediatype == 'AUDIO') {
            if ($mediathumb == '') {
                echo "\t\t\t\t\t<img src=\"".$website_url."dash/images/generic-audio-600.png\" class=\"w3-container w3-image\">\n";
            } else {
                echo "\t\t\t\t\t<img src=\"".$mediathumb."\" class=\"w3-container w3-image\"";
                if ($mediathumbcw == "1") {
                    echo " style=\"filter:blur(25px)\"";
                }
                echo ">\n";
            }
        } else if ($mediatype == 'VIDEO') {
            if ($mediathumb == '') {
                echo "\t\t\t\t\t<img src=\"".$website_url."dash/images/generic-video-600.png\" class=\"w3-container w3-image\">\n";
            } else {
                echo "\t\t\t\t\t<img src=\"".$mediathumb."\" class=\"w3-container w3-image\"";
                if ($mediathumbcw == "1") {
                    echo " style=\"filter:blur(25px)\"";
                }
                echo ">\n";
            }
        }
        echo "\t\t\t\t</div>\n";
        echo "\t\t\t</article>\n";
    }
} else {
    echo "\t\t\t<article class=\"w3-content w3-padding\">"._("There is currently no content.")."</article>\n";
}

?>
            </div>
<?php
    echo "\t\t\t<table cellspacing=\"2\" cellpadding=\"2\" align=\"center\">\n";
    echo "\t\t\t\t<tbody>\n";
    echo "\t\t\t\t\t<tr>\n";
    echo "\t\t\t\t\t\t<td align=\"center\">\n";

    if (isset($pg)) {

        /**
         * Get the total number of rows in the table
         */
        $pagecountquery = mysqli_query($dbconn,"SELECT COUNT(*) AS Total FROM ".TBLPREFIX."media");
        $pagecount = mysqli_num_rows($pagecountquery);

        if ($pagecount) {
            $rs = mysqli_fetch_assoc($pagecountquery);
            $total = $rs["Total"];
        }

        /**
         * Calculate the total number of pages
         * The total number of pages is the number of items in the media table divided by the number of items per page
         */
        $totalPages = ceil($total / $perpg);

        /**
         * Define $prev, $next, $adjc
         */
        $prev = $pg - 1;
        $next = $pg + 1;
        $adjc = 3;
        $tpm1 = $totalPages - 1;

        /**
         * PREV section
         * If the page number is 1, there are no previous pages to link to
         */
        if($pg <=1 ){
            echo "\t\t\t\t\t\t\t<span class=\"w3-padding w3-disabled\">"._('PREV')."</span>\n";
        } else {
            echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$prev'>"._('PREV')."</a></span>\n";
        }

        /**
         * Numbered section
         * If there are too few pages to bother breaking up
         */
        if ($totalPages < 7 + ($adjc * 2 )) {
            for($i = 1; $i <= $totalPages; $i++) {
                if($i<>$pg) {
                    echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$i'>$i</a></span>\n";
                } else {
                    echo "\t\t\t\t\t\t\t<span class=\"w3-button w3-green\" style='font-weight: bold;'>$i</span>\n";
                }
            }


        }

        /**
         * if there enough pages to break into sections
         */
        else if ($totalPages > 5 + ($adjc * 2))

        {

            /**
             * If it's close to the beginning, we only hide pages near the end
             */
            if ($pg < 1 + ($adjc * 2))

            {
                for ($i = 1; $i < 4 + ($adjc * 2); $i++) {
                    if($i<>$pg) {
                        echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$i'>$i</a></span>\n";
                    } else {
                        echo "\t\t\t\t\t\t\t<span class=\"w3-button w3-green\" style='font-weight: bold;'>$i</span>\n";
                    }
                }
                echo "\t\t\t\t\t\t\t<span>...</span>\n";
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$tpm1'>$tpm1</a></span>\n";
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$totalPages'>$totalPages</a></span>\n";
            }

            /**
             * If we are in the middle, hide some near the beginning and the end
             */
            else if ($totalPages - ($adjc * 2) > $pg && $pg > ($adjc * 2))

            {
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=1'>1</a></span>\n";
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=2'>2</a></span>\n";
                echo "\t\t\t\t\t\t\t<span>...</span>\n";
                for ($i = $pg - $adjc; $i <= $pg + $adjc; $i++) {
                    if($i<>$pg) {
                        echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$i'>$i</a></span>\n";
                    } else {
                        echo "\t\t\t\t\t\t\t<span class=\"w3-button w3-green\" style='font-weight: bold;'>$i</span>\n";
                    }
                }
                echo "\t\t\t\t\t\t\t<span>...</span>\n";
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$tpm1'>$tpm1</a></span>\n";
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$totalPages'>$totalPages</a></span>\n";
            }

            /**
             * If we are near the end, we only hide some near the beginning
             */
            else

            {
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=1'>1</a></span>\n";
                echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=2'>2</a></span>\n";
                echo "\t\t\t\t\t\t\t<span>...</span>\n";
                for ($i = $totalPages - (2 + ($adjc * 2)); $i <= $totalPages; $i++) {
                    if($i<>$pg) {
                        echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$i'>$i</a></span>\n";
                    } else {
                        echo "\t\t\t\t\t\t\t<span class=\"w3-button w3-green\" style='font-weight: bold;'>$i</span>\n";
                    }
                }
            }
        }



        /**
         * NEXT section
         * If the page number is the same as the total number of pages, there are no subsequent pages to link to
         */
        if($pg == $totalPages ) {
            echo "\t\t\t\t\t\t\t<span class=\"w3-padding w3-disabled\">"._('NEXT')."</span>\n";
        } else {
            echo "\t\t\t\t\t\t\t<span><a class=\"w3-button\" href='?pg=$next'>"._('NEXT')."</a></span>\n";
        }
    }

    echo "\t\t\t\t\t\t</td>\n";
    echo "\t\t\t\t\t</tr>\n";
    echo "\t\t\t\t</tbody>\n";
    echo "\t\t\t</table>\n";

?>
<?php
include_once "includes/fed-footer.php";
?>
