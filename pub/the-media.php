<?php
/**
 * pub/the-media.php
 *
 * Displays a media item
 * Right now (22 Jan 2020) is will only show an image.
 * In the future, it should handle multiple media types
 *
 * since Hobgoblin version 0.1
 */

include_once    "../conn.php";
include         "../functions.php";
require         "includes/database-connect.php";
require_once    "includes/configuration-data.php";
include_once    "nodeinfo/version.php";


// get the ID or title of the page on this webpage
if (isset($_GET["mdid"])) {
    $get_id = $_GET["mdid"];
} else if (isset($_GET["title"])) {
    $get_title = $_GET["title"];
} else {
    $get_id = "";
    $get_title = "";
}


if ($get_id != '') {

    $medq = "SELECT * FROM ".TBLPREFIX."media WHERE media_id=\"".$get_id."\"";
    $medquery = mysqli_query($dbconn,$medqq);
    while($med_opt = mysqli_fetch_assoc($medqueryquery)) {
        $mediaid        = $med_opt['media_id'];
        $mediatitle     = $med_opt['media_title'];
        $mediaslug      = $med_opt['media_slug'];
        $mediaby        = $med_opt['user_name'];
        $mediatime      = $med_opt['media_date'];
        $mediaalt       = htmlspecialchars_decode($med_opt['media_alt']);
        $mediacapt      = htmlspecialchars_decode($med_opt['media_caption']);
        $mediadesc      = htmlspecialchars_decode($med_opt['media_description']);
        $mediatags      = $med_opt['media_tags'];
        $mediacats      = $med_opt['media_categories'];
        $mediastat      = $med_opt['media_status'];
        $mediatype      = $med_opt['media_type'];
        $mediathumb     = urldecode($med_opt['media_thumbnail_url']);
        $mediamodd      = $med_opt['media_modified_date'];
        $mediaurl       = $med_opt['media_url'];
    }

    $by_q = "SELECT * FROM ".TBLPREFIX."users WHERE user_name=\"".$mediaby."\"";
    $by_query = mysqli_query($dbconn,$by_q);
    while($by_opt = mysqli_fetch_assoc($by_query)) {
        $byname     = $by_opt['user_name'];
    }

} else if ($get_title != '') {
    $medq = "SELECT * FROM ".TBLPREFIX."media WHERE media_slug=\"".$get_title."\"";
    $medquery = mysqli_query($dbconn,$medq);
    while($med_opt = mysqli_fetch_assoc($medquery)) {
        $mediaid        = $med_opt['media_id'];
        $mediatitle     = $med_opt['media_title'];
        $mediaslug      = $med_opt['media_slug'];
        $mediaby        = $med_opt['user_name'];
        $mediatime      = $med_opt['media_date'];
        $mediaalt       = $med_opt['media_alt'];
        $mediacapt      = htmlspecialchars_decode($med_opt['media_caption']);
        $mediadesc      = htmlspecialchars_decode($med_opt['media_description']);
        $mediatags      = $med_opt['media_tags'];
        $mediacats      = $med_opt['media_categories'];
        $mediastat      = $med_opt['media_status'];
        $mediatype      = $med_opt['media_type'];
        $mediathumb     = urldecode($med_opt['media_thumbnail_url']);
        $mediamodd      = $med_opt['media_modified_date'];
        $mediaurl       = $med_opt['media_url'];
    }


    // split tags & categories
    if ($mediatags !== "") {
        $tags = preg_split('/,/',$mediatags);
    } else {
        $tags = "";
    }

    if ($mediacats !== "") {
        $cats = preg_split('/,/',$mediacats);
    } else {
        $cats = "";
    }

    $by_q = "SELECT * FROM ".TBLPREFIX."users WHERE user_name=\"".$mediaby."\"";
    $by_query = mysqli_query($dbconn,$by_q);
    while($by_opt = mysqli_fetch_assoc($by_query)) {
        $byname     = $by_opt['user_name'];
    }
}

$pagetitle = $mediatitle;
include_once "includes/fed-header.php";
include_once "includes/fed-nav.php";
?>

<?php
            echo "\t\t\t<article role=\"article\" class=\"w3-col w3-panel w3-cell m10\">\n";
            echo "\t\t\t\t<h2 role=\"heading\" class=\"w3-text-theme w3-container w3-bar\">".$mediatitle."</h2>\n";
            echo "\t\t\t\t<span class=\"w3-container w3-block\">"._('Posted on ').$mediatime._(' by ')."<a href=\"".$website_url."users/".$mediaby."/\">".$byname."</a></span><br>\n";
            echo "\t\t\t\t<div class=\"w3-card\">\n";

if ($mediatype == 'IMAGE') {
    echo "\t\t\t\t\t<img src=\"".$website_url.$mediaurl."\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-image\">\n";
    echo "\t\t\t\t\t<div class=\"w3-container\">\n";
    echo "\t\t\t\t\t\t<h5>".$mediacapt."</h5>\n";
    echo "\t\t\t\t\t\t<p>".$mediadesc."</p>\n";
    echo "\t\t\t\t\t</div> <!-- end div w3-container -->\n";
} else if ($mediatype == 'VIDEO') {
    echo "\t\t\t\t\t<video controls class=\"w3-container w3-image\">\n";
    echo "\t\t\t\t\t\t<source src=\"".$website_url.$mediaurl."\">\n";
    echo "\t\t\t\t\t</video><br><br>\n";
    echo "\t\t\t\t\t<div class=\"w3-container\">\n";
    echo "\t\t\t\t\t\t<p>".$mediadesc."</p>\n";
    echo "\t\t\t\t\t</div> <!-- end div w3-container -->\n";
} else if ($mediatype == 'AUDIO') {
    if ($mediathumb == '') {
        echo "\t\t\t\t\t<img src=\"".$website_url."dash/images/generic-audio-600.png\" width=\"600\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"><br><br>\n";
    } else {
        echo "\t\t\t\t\t<img src=\"".$mediathumb."\" width=\"600\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"><br><br>\n";
    }
    echo "\t\t\t\t\t<audio style=\"width:600px\" controls class=\"w3-container w3-image\">\n";
    echo "\t\t\t\t\t\t<source src=\"".$website_url.$mediaurl."\">\n";
    echo "\t\t\t\t\t</audio><br><br>\n";
    echo "\t\t\t\t\t<div class=\"w3-container\">\n";
    echo "\t\t\t\t\t\t<p>".$mediadesc."</p>\n";
    echo "\t\t\t\t\t</div> <!-- end div w3-container -->\n";
}
            echo "\t\t\t\t</div> <!-- end div w3-card -->\n";

// Only show this if we have tags to show
if ($tags !== "") {
    echo "\t\t\t\t<p class=\"w3-container\">"._('Tags: ');
    foreach ($tags as $tag) {
        echo "<a href=\"".$website_url."tag/".makeslug($tag)."\" class=\"w3-tag w3-blue\">".trim($tag)."</a>&nbsp;";
    }
    echo "</p>\n";
}

// Only show this if we have categories to show
if ($cats !== "") {
    echo "\t\t\t\t<p class=\"w3-container\">"._('Categories: ');
    foreach ($cats as $cat) {
        echo "<a href=\"".$website_url."category/".makeslug($cat)."\" class=\"w3-tag w3-purple\">".trim($cat)."</a>&nbsp;";
    }
    echo "</p>\n";
}
            echo "\t\t\t</article>\n";
?>

<?php
include_once "includes/fed-footer.php";
?>
