<?php
/**
 * pub/the-artist.php
 *
 * Displays information about an artist and their works
 *
 * since Hobgoblin 0.1
 */

include_once    "../conn.php";
include         "../functions.php";
require         "includes/database-connect.php";
require_once    "includes/configuration-data.php";
include_once    "nodeinfo/version.php";



// get the artist info
if (isset($_GET["name"])) {
    $name = rtrim($_GET["name"],"/");
} else {
    $name = "";
}


if ($name != '') {

    $artistq = "SELECT * FROM ".TBLPREFIX."artists WHERE artist_slug=\"".$name."\"";
    $artistquery = mysqli_query($dbconn,$artistq);
    while($artistopt = mysqli_fetch_assoc($artistquery)) {
        $artistid       = $artistopt['artist_id'];
        $artistname     = retext($artistopt['artist_name']);
        $artistavtr     = urldecode($artistopt['artist_avatar_url']);
        $artistdesc     = retext($artistopt['artist_description']);
        $artistdob      = $artistopt['artist_date_of_birth'];
        $artistdod      = $artistopt['artist_date_of_death'];
        $artistpob      = retext($artistopt['artist_place_of_birth']);
        $artistpod      = retext($artistopt['artist_place_of_death']);
        $artistcod      = retext($artistopt['artist_cause_of_death']);
        $artistnat      = retext($artistopt['artist_nationality']);
        $artistoccu     = retext($artistopt['artist_occupation']);
        $artistasso     = retext($artistopt['artist_associates']);
    }
}

$pagetitle = $artistname." « ".$website_name;
include_once 'includes/fed-header.php';
include_once 'includes/fed-nav.php';
?>
            <div class="w3-col w3-panel w3-cell m10">

                <!-- This section displays some information about the artist -->
                <article class="w3-panel w3-theme-d5">
<?php
    if ($artistavtr !== '') {
        echo "\t\t\t\t\t<aside class=\"w3-right w3-padding-16\">\n";
        echo "\t\t\t\t\t\t<img src=\"".$website_url.$artistavtr."\" class=\"avatar\" alt=\""._("Avatar for ").$artistname."\" title=\""._("Avatar for ").$artistname."\"><br>\n";
        echo "\t\t\t\t\t\t<p class=\"w3-center\">"._("Avatar for ").$artistname."</p>\n";
        echo "\t\t\t\t\t</aside>\n";
    }
?>
                    <h2 class="w3-text-theme"><?php echo $artistname; ?></h2>
                    <?php echo $artistdesc; ?><br>
                </article>

                <!-- Everything below should be works created by the artist -->
<?php
// let's get all media, then display the ones where the artist is listed.
$med_q = "SELECT * FROM ".TBLPREFIX."media WHERE media_status=\"PUBLIC\" ORDER BY media_date DESC";
$med_query = mysqli_query($dbconn,$med_q);
if (mysqli_num_rows($med_query) <> 0) {
    while ($med_opt = mysqli_fetch_assoc($med_query)) {
        $mediaid        = $med_opt['media_id'];
        $mediauser      = $med_opt['user_name'];
        $mediadate      = $med_opt['media_date'];
        $mediatitle     = retext($med_opt['media_title']);
        $mediaslug      = $med_opt['media_slug'];
        $mediaalt       = retext($med_opt['media_alt']);
        $mediaurl       = $med_opt['media_url'];
        $mediathumb     = urldecode($med_opt['media_thumbnail_url']);
        $mediathumbcw   = $med_opt['media_thumbnail_warning'];
        $mediatype      = $med_opt['media_type'];
        $mediawarn      = $med_opt['media_content_warning'];
        $mediaarti      = preg_split('/,/i', retext($med_opt['media_artist']));


        /**
         * Let's get these sorted
         * We only want to show works by this creator
         */
        foreach ($mediaarti as $artist) {
            if ($artist == $artistname) {
                echo "\t\t\t\t<article role=\"article\" class=\"w3-panel\">\n";
                echo "\t\t\t\t\t<h2 class=\"w3-text-theme w3-container w3-bar\"><a href=\"".$website_url."media/".$mediaslug."\">".$mediatitle."</a></h2>\n";
                echo "\t\t\t\t\t<span class=\"w3-container w3-block\">"._('Posted on ').$mediadate._(' by ').$mediauser."</span><br>\n";
                echo "\t\t\t\t\t<div class=\"w3-container w3-block\">\n";
                /**
                 * Change the view depending on the media type
                 */
                if ($mediatype == 'IMAGE') {
                    echo "\t\t\t\t\t<img src=\"".$website_url.$mediaurl."\" alt=\"".$mediaalt."\" title=\"".$mediaalt."\" class=\"w3-container w3-image\"";
                    if ($mediawarn == "1") {
                        echo " style=\"filter:blur(25px)\"";
                    }
                    echo "><br><br>\n";
                } else if ($mediatype == 'AUDIO') {
                    if ($mediathumb == '') {
                        echo "\t\t\t\t\t<img src=\"".$website_url."dash/images/generic-audio-600.png\" class=\"w3-container w3-image\"></a>\n";
                    } else {
                        echo "\t\t\t\t\t<img src=\"".$website_url.$mediathumb."\" class=\"w3-container w3-image\"></a>\n";
                    }
                } else if ($mediatype == 'VIDEO') {
                    if ($mediathumb == '') {
                        echo "\t\t\t\t\t<img src=\"".$website_url."dash/images/generic-video-600.png\" class=\"w3-container w3-image\"></a>\n";
                    } else {
                        echo "\t\t\t\t\t<img src=\"".$website_url.$mediathumb."\" class=\"w3-container w3-image\"></a>\n";
                    }
            }
                echo "\t\t\t\t\t</div>\n";
                echo "\t\t\t\t</article>\n";
            }
        }

    }
} else {
    echo "\t\t\t\t<div class=\"w3-card-2 w3-theme-l3 w3-padding w3-margin-bottom\">\n";
    echo _("There are no posts at the moment")."<br>\n";
    echo $usrq."<br>\n";
    echo $pst_q."<br>\n";
    echo "\t\t\t\t</div>\n";
}
?>
            </div> <!-- div class="w3-col w3-panel w3-cell m8" -->

            <div class="w3-col w3-cell m3">&nbsp;</div>
    </div> <!-- end THE GRID -->
<?php
include_once "includes/fed-footer.php";
?>
